export default {
    primaryColor: '#0D1847',
    white: '#ffffff',
    lightGrey: '#edf2f5',
    red: '#d45142',
};
