import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { Button, Card, CardBody, Col, Collapse, Form, Input, Label, Modal, ModalBody, ModalHeader, Row, Spinner } from 'reactstrap'
import { useAuthUser } from '../../../store'
import { useMediaQuery } from 'react-responsive'
import request from '../../../utils/request';
import ModalError from '../../../components/ModalError';
import LoadingAnimation from '../../../components/LoadingAnimation';
import { Link, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import * as Yup from 'yup';
import profilePhotoNotFound from '../../../assets/img/no-photo.png';
import noProject from '../../../assets/img/no-project.png';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';
import useSWR from 'swr';
import { badgeStatus } from '../Project/ProjectCard';
import { MemberItem } from '../Project/ProjectDetail';
import ProjectList from './ProjectList';

const noDataDesc = {
    pending: 'belum terverifikasi',
    approved: 'disetujui',
    rejected: 'ditolak'
}

function Profile() {
    const inputFile = useRef(null)
    const user = useAuthUser();
    const location = useLocation()
    const search = new URLSearchParams(location.search);
    const [loading, setLoading] = useState(false)
    const [deleting, setDeleting] = useState(false)
    const [data, setData] = useState([])
    const [error, setError] = useState(false)
    const [edit, setEdit] = useState(false);
    const [teamList, setTeamList] = useState(false);
    const [projectList, setProjectList] = useState(true);
    const [projectAll, setProjectAll] = useState(false)
    const isSmallSize = useMediaQuery({ query: '(max-width: 768px)' })
    const [hasAction, setHasAction] = useState(false)
    const [filter, setFilter] = useState('approved')
    const [local, setLocal] = useState({
        fullName: null,
        phoneNumber: null,
        photo: null,
        preview: null
    })
    const { data: getMyTeamApproved } = useSWR(`v1/teams/me`, { refreshInterval: 0 });
    const dataMyTeamApproved = useMemo(() => getMyTeamApproved?.data?.data ?? [], [getMyTeamApproved]);
    const { data: getMyTeam, error: getMyTeamError } = useSWR(`v1/teams/me?status=${filter}`, { refreshInterval: 15000 });
    const dataMyTeam = useMemo(() => getMyTeam?.data?.data ?? [], [getMyTeam]);
    const loadingMyTeam = !getMyTeam || getMyTeamError
    const [modalStatusMessage, setModalStatusMessage] = useState({
        header: '',
        statusMessage: '',
        isOpen: false,
    })
    const toggleModalStatusMessage = (data) => {
        setModalStatusMessage({ header: data?.lead?.leadName, statusMessage: data?.statusMessage, isOpen: !modalStatusMessage.isOpen });
    }

    const seePending = useCallback(() => {
        setFilter('pending')
    }, [])
    const seeVerif = useCallback(() => {
        setFilter('approved')
    }, [])
    const seeReject = useCallback(() => {
        setFilter('rejected')
    }, [])

    const ValidationFormSchema = useMemo(() => {
        return Yup.object().shape({
            fullName: Yup.string().required().label('Nama Lengkap'),
            phoneNumber: Yup.string().label('Nomor Handphone')
        })
    }, [])

    const { values, touched, errors, isSubmitting, ...formik } = useFormik({
        initialValues: {
            fullName: user.detail.fullName,
            phoneNumber: user.detail.phoneNumber,
            photo: null,
            preview: user.detail.photo
        },
        validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true)
            let form = new FormData();
            form.append('fullName', values.fullName)
            form.append('phoneNumber', values.phoneNumber)
            if (values.photo) form.append('photo', values.photo, values.photo.name)
            request.put('v1/auth/profile', form)
                .then(() => {
                    setHasAction(true)
                    formik.setValues({
                        fullName: values.fullName,
                        phoneNumber: values.phoneNumber,
                        photo: values.photo,
                        preview: values.preview
                    })
                    setLocal({
                        fullName: values.fullName,
                        phoneNumber: values.phoneNumber,
                        photo: values.photo,
                        preview: values.preview
                    })
                    toast.success('Berhasil mengubah Profil')
                    // dispatch(getMe());
                    homeProfile()
                })
                .catch(() => {
                    toast.error('Gagal mengubah Foto Profil')
                    return;
                })
                .finally(() => setSubmitting(false))
        }
    })

    useEffect(() => {
        setLoading(true)
        request.get('v1/projects/me')
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => setError(true))
            .finally(() => setLoading(false))
    }, [])

    const handleNumberOnly = (evt) => {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault()
        }
        return true;
    }
    const doEdit = () => {
        setEdit(true)
        setTeamList(false)
        setProjectList(false)
        setProjectAll(false)
    }
    const seeProject = useCallback(() => {
        setEdit(false)
        setTeamList(false)
        setProjectList(false)
        setProjectAll(true)
    }, [])
    const seeTeam = useCallback(() => {
        if (!teamList) seePending()
        if (!teamList && search.get('show') === 'teams') {
            seeReject()
        }
        if (!teamList && search.get('show') === 'myteams') {
            seeVerif()
        }
        setEdit(false)
        setTeamList(true)
        setProjectList(false)
        setProjectAll(false)
    }, [teamList, search, seeReject, seePending, seeVerif])

    const homeProfile = () => {
        setEdit(false)
        setTeamList(false)
        setProjectList(true)
        setProjectAll(false)
    }

    useEffect(() => {
        if ((search.get('show') === 'teams' || search.get('show') === 'myteams')) {
            seeTeam()
        }

        if (search.get('show') === 'projects') {
            seeProject()
        }
    }, [search, dataMyTeam, seeTeam, seeProject]);

    const onButtonClick = () => {
        inputFile.current.click();
    }

    const onChangeFile = (e) => {
        e.preventDefault();
        const data = e.target.files[0]
        if (data.size > 5242880) {
            toast.error('Foto melebihi ukuran maksimal')
            return;
        }
        formik.setFieldValue('preview', URL.createObjectURL(e.target.files[0]))
        formik.setFieldValue('photo', e.target.files[0])
    }

    const deletePhoto = () => {
        setDeleting(true)
        request.delete('v1/auth/profile/photo')
            .then(() => {
                formik.setFieldValue('photo', null)
                formik.setFieldValue('preview', null)
            })
            .catch(() => toast.error('Gagal menghapus foto profil'))
            .finally(() => setDeleting(false))
    }

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    const onErrorProject = (e) => {
        e.target.src = noProject;
        e.target.onerror = null;
    }

    if (loading) {
        return <LoadingAnimation />
    }
    if (error) {
        return <ModalError />
    }

    return (
        <Card className="border-0 mb-5 mb-md-0" style={{ borderRadius: '5px', position: 'relative' }}>
            <Form onSubmit={formik.handleSubmit}>
                <div className="absolute-right">
                    {!edit &&
                        <Button color="netis-color" className="px-2" onClick={doEdit}>
                            <i className="fa fa-pencil mr-2" />Edit
                        </Button>
                    }
                </div>
                <div className="text-center py-4">
                    <div className="rounded-circle frame-profile-picture-empty mb-3 d-flex justify-content-center align-items-center">
                        {values?.preview ?
                            <img src={values?.preview} alt="profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                            :
                            <DefaultProfile font='big-font-initial' init={user.detail.fullName} size={isSmallSize ? '100px' : '160px'} />
                        }
                        {edit &&
                            <>
                                <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} onChange={(e) => onChangeFile(e)} accept="image/png, image/gif, image/jpeg" />
                                <Button
                                    className="btn border-0 rounded-circle img-profile-button d-flex flex-column justify-content-center"
                                    style={{ position: 'absolute' }}
                                    onClick={onButtonClick}
                                >
                                    <i className="fa fa-2x fa-camera" />
                                    <div className="desc-img-profile text-center d-none d-md-block">
                                        Unggah Foto Profil<br />
                                        <small className="text-danger">Maks. 5 MB</small>
                                    </div>
                                </Button>
                            </>
                        }
                    </div>
                    {(edit && values.photo) &&
                        <Button disabled={deleting} onClick={deletePhoto} style={{ border: 0 }} className="btn btn-sm bg-transparent text-danger">
                            <i className="fa fa-trash" /> Hapus Foto Profil
                        </Button>
                    }
                    <h2 className="mb-5">
                        {values.fullName}
                    </h2>
                    <Collapse isOpen={!edit} className="d-flex justify-content-center">
                        <div className="d-flex justify-content-around profile-list">
                            <div className={`text-center mr-1 hover-pointer rounded ${projectAll ? `active-profile-list` : ``}`} onClick={seeProject}>
                                <h3 style={{ lineHeight: 0.5 }}>{data?.length}</h3>Proyek
                            </div>
                            {(!projectList && !edit) &&
                                <Link className="text-center mx-1 hover-pointer text-dark" to="/profile" onClick={homeProfile}>
                                    <h3 style={{ lineHeight: 0.5 }}><i className="fa fa-th" /></h3>
                                    <small className="text-muted"><i><i className="fa fa-arrow-left" />back to list</i></small>
                                </Link>
                            }
                            <div className={`text-center ml-1 hover-pointer rounded ${teamList ? `active-profile-list` : ``}`} onClick={seeTeam}>
                                <h3 style={{ lineHeight: 0.5 }}>{dataMyTeamApproved?.length}</h3>Tim
                            </div>
                        </div>
                    </Collapse>
                </div>
                <CardBody>
                    <Collapse isOpen={edit}>
                        <Row className="mt-1 input-form">
                            <Col sm="6" className="mb-3">
                                <Label htmlFor="fullName" className="input-label">Nama Lengkap<span className="required">*</span></Label>
                                <Input
                                    className="form-control"
                                    type="input"
                                    value={values?.fullName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    name="fullName"
                                    id="fullName"
                                    maxLength="255"
                                    placeholder="Nama Lengkap"
                                />
                                {touched.fullName && errors.fullName && <small className="text-danger">{errors.fullName}</small>}
                            </Col>
                            <Col sm="6" className="mb-3">
                                <Label htmlFor="phoneNumber" className="input-label">No. HP<span className="required">*</span></Label>
                                <Input
                                    onKeyPress={handleNumberOnly}
                                    value={values?.phoneNumber}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    pattern="[0-9]*"
                                    inputMode="numeric"
                                    type="text"
                                    className="form-control"
                                    name="phoneNumber"
                                    id="phoneNumber"
                                    placeholder="No. HP*"
                                />
                                {touched.phoneNumber && errors.phoneNumber && <small className="text-danger">{errors.phoneNumber}</small>}
                            </Col>
                        </Row>
                        <div className="d-flex justify-content-end ml-auto">
                            <Button
                                disabled={isSubmitting}
                                className="mr-2"
                                color="netis-secondary"
                                onClick={() => {
                                    if (!hasAction) {
                                        formik.handleReset();
                                    }
                                    else if (hasAction) {
                                        formik.setValues({
                                            fullName: local.fullName,
                                            phoneNumber: local.phoneNumber,
                                            photo: local.photo,
                                            preview: local.preview
                                        })
                                    }
                                    homeProfile();
                                }}
                            >
                                Batal
                            </Button>
                            <Button type="submit" disabled={isSubmitting} className="ml-2" color="netis-color">
                                {isSubmitting ? <><Spinner color="light" size="sm" /> loading...</> : 'Simpan'}
                            </Button>
                        </div>
                    </Collapse>
                    <Collapse isOpen={projectList}>
                        <Row>
                            {data?.filter(p => p.verified === 'verified').map((item, idx) =>
                                <Col xs="4" key={idx} className={`p-1`}>
                                    <Link to={`/project/${item.code}`}>
                                        <div className={`frame-project ${!isSmallSize && `scale-div-small`} box`}>
                                            {item?.media[0]?.storage &&
                                                <img src={item?.media[0]?.storage} alt="myProject" className="img img-responsive full-width" onError={(e) => onErrorProject(e)} />
                                            }
                                        </div>
                                    </Link>
                                </Col>
                            )}
                        </Row>
                    </Collapse>
                    <Collapse isOpen={teamList}>
                        <Row>
                            <Col xs="12" className="d-flex justify-content-center mb-3">
                                <Button onClick={seePending} color={filter === 'pending' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                                    Belum Verifikasi
                                </Button>
                                <Button onClick={seeVerif} color={filter === 'approved' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                                    Disetujui
                                </Button>
                                <Button onClick={seeReject} color={filter === 'rejected' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                                    Ditolak
                                </Button>
                            </Col>
                            {loadingMyTeam ?
                                <Col xs="12" className="pt-4 mx-auto text-center"
                                    style={{
                                        display: "flex",
                                        justifyContent: "center",
                                        alignItems: "center",
                                    }}
                                >
                                    <Spinner style={{ width: 48, height: 48 }} />
                                </Col>
                                :
                                (dataMyTeam?.length < 1 ?
                                    <Col xs="12" className="pt-4 mx-auto text-center">
                                        Belum ada Tim yang {noDataDesc[filter]}
                                    </Col>
                                    : dataMyTeam?.map((item, i) => (
                                        <Col xs="12" md="6" key={i} className={`p-2`}>
                                            <Link
                                                to={{
                                                    pathname: item.status === `rejected` ? `` : `/project/${item.project.code}/team/${item.id}`,
                                                    state: { team: item.lead.leadName }
                                                }}
                                                className={item.status === 'rejected' ? 'reject-link' : ''}
                                                style={{ color: 'black' }}
                                                onClick={() => item.status === `rejected` ? toggleModalStatusMessage(item) : ''}
                                            >
                                                <Card className="card-team-review mb-1" style={{ borderRadius: '5px' }}>
                                                    <CardBody>
                                                        <Row className="card-team-info">
                                                            <Col xs="12" className="d-flex mb-2">
                                                                <img src={item?.project.imagePreview.url} alt="profile" className="project-photo-review rounded" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                                                <div>
                                                                    <h6 className="font-weight-bold ml-3">{item.project.title}</h6>
                                                                    <h6 className="text-muted ml-3 mb-0">Tim {item.lead.leadName}</h6>
                                                                </div>
                                                            </Col>
                                                            <Col xs="6">
                                                                <div className="d-flex flex-column flex-lg-fill float-left">
                                                                    <span className="text-muted text-left">Status Proyek</span>
                                                                    <div className="d-flex align-items-center mt-2">
                                                                        {badgeStatus(item.project.status)}
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                            <Col xs="6">
                                                                <div className="d-flex flex-column flex-lg-fill float-right">
                                                                    <span className="text-muted text-right">Members</span>
                                                                    <div className="symbol-group symbol-hover">
                                                                        {item.members.filter(m => item.status === 'rejected' ? m.status === 'rejected' : (item.status === 'approved' ? m.status === 'approved' : m.status === 'pending')).map((member, k) => (
                                                                            k >= 3 ? null :
                                                                                <MemberItem member={member} project={item.project} key={k} />
                                                                        ))}
                                                                        {item.members.length > 3 &&
                                                                            <div className="symbol symbol-30 symbol-circle symbol-light">
                                                                                <span className="symbol-label font-weight-bold">{item.members.length - 3}+</span>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                        </Row>
                                                    </CardBody>
                                                </Card>
                                            </Link>
                                        </Col>
                                    )))}
                        </Row>
                    </Collapse>
                    <Collapse isOpen={projectAll}>
                        <ProjectList data={data} />
                    </Collapse>
                </CardBody>
            </Form>
            <ModalStatusMessage data={modalStatusMessage} toggle={() => toggleModalStatusMessage('')} />
        </Card>
    )
}

const ModalStatusMessage = ({ data, toggle }) => {
    return (
        <Modal toggle={toggle} isOpen={data?.isOpen} modalClassName="d-flex align-items-center" className="d-flex justify-content-center w-100">
            <ModalHeader toggle={toggle}>
                <div className="font-lg font-weight-bold">{data.header}</div>
            </ModalHeader>
            <ModalBody>
                <Row>
                    <Col xs="12" className="mb-3">
                        {data.statusMessage ?? 'Tidak ada pesan'}
                    </Col>
                </Row>
            </ModalBody>
        </Modal>
    )
}

export default Profile