import React, { useState } from 'react';
import { Alert, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Modal, ModalBody, ModalHeader, Row } from 'reactstrap';

export default ({ data, toggle }) => {
    const [visible, setVisible] = useState(false);

    const onDismiss = () => setVisible(!visible);

    return (
        <Modal toggle={toggle} isOpen={data?.isOpen} modalClassName="d-flex align-items-center" className="d-flex justify-content-center w-100">
            <ModalHeader toggle={toggle}>
                <div className="font-lg font-weight-bold">Copy link</div>
            </ModalHeader>
            <ModalBody>
                <Row>
                    <Col xs="12" className="mb-3">
                        <InputGroup>
                            <Input readOnly placeholder="Link..." type="text" value={data?.link} />
                            <InputGroupAddon onClick={() => { navigator.clipboard.writeText(data?.link); onDismiss() }} addonType="append" style={{ cursor: 'pointer' }}>
                                <InputGroupText className="bg-primary">Copy</InputGroupText>
                            </InputGroupAddon>
                        </InputGroup>
                    </Col>
                    <Col xs="12">
                        <Alert color="info" isOpen={visible} toggle={onDismiss}>
                            Link berhasil dicopy ke clipboard!
                        </Alert>
                    </Col>
                </Row>
            </ModalBody>
        </Modal>
    )
}