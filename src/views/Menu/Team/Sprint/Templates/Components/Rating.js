import React, { useMemo, useState, useEffect, memo } from "react";
import { Row, Col, Progress, Spinner } from "reactstrap";
import { useAuthUser } from "../../../../../../store";
import ReactStars from "react-rating-stars-component";

export default memo(({ matchRoute, socket, cardId, write }) => {
    const user = useAuthUser();
    const [loading, setLoading] = useState(true);
    const [hasRated, setHasRated] = useState(null);
    const [data, setData] = useState([]);

    useEffect(() => {
        socket.emit("joinRatingCard", { cardId }, (res) => {
            if (!res.success) {
                console.log('Socket Error')
            } else {
                // setLoading(false)
            }
        });
        socket.on('getRatingCard', (res) => {
            setData(res.data)
            Object.values(res.data).map((v, i) => v.filter((r, k) => r.userId === user.id ? setHasRated(r) : []))
            setLoading(false)
        })
        // eslint-disable-next-line
    }, [])

    const postRating = (rate) => {
        socket.emit('postRating', { rate, cardId, teamId: matchRoute.params.teamId }, (res) => {
            if(res.success){
                setHasRated(true)
            }
        })
    }

    // useEffect(() => {
    //     if (data) {
    //     };
    // }, [data, user])

    const rate1 = useMemo(() => data[1] ?? [], [data])
    const rate2 = useMemo(() => data[2] ?? [], [data])
    const rate3 = useMemo(() => data[3] ?? [], [data])
    const rate4 = useMemo(() => data[4] ?? [], [data])
    const rate5 = useMemo(() => data[5] ?? [], [data])

    const rateCount = useMemo(() => parseInt(rate5.length) + parseInt(rate4.length) + parseInt(rate3.length) + parseInt(rate2.length) + parseInt(rate1.length), [rate5, rate4, rate3, rate2, rate1])
    const rateAmount = useMemo(() => ((5 * rate5.length) + (4 * rate4.length) + (3 * rate3.length) + (2 * rate2.length) + (1 * rate1.length)) / rateCount, [rate5, rate4, rate3, rate2, rate1, rateCount])

    const ratePercents = useMemo(() => (
        {
            1: rate1.length / rateCount * 100,
            2: rate2.length / rateCount * 100,
            3: rate3.length / rateCount * 100,
            4: rate4.length / rateCount * 100,
            5: rate5.length / rateCount * 100,
        }
    ), [rate5, rate4, rate3, rate2, rate1, rateCount])

    if (loading) {
        return (
            <div
                style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                    background: "rgba(255,255,255, 0.5)",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Spinner style={{ width: 48, height: 48 }} />
            </div>
        )
    }

    return (
        <>
            <Row style={{ padding: '0px 1rem' }}>
                <Col xs="12" className="px-0">
                    <div className="font-weight-bold font-lg">Penilaian</div>
                </Col>
                <Col xs="4" className="px-0 text-center">
                    <div className="font-weight-bold" style={{ fontSize: '50pt', lineHeight: 1 }}>{rateAmount ? (Math.round(rateAmount * 100) / 100).toFixed(1) : 0}</div>
                    <div style={{ fontSize: '12pt' }}>dari 5</div>
                </Col>
                <Col xs="8" className="px-0 d-flex justify-content-center">
                    <div className="w-100" style={{ marginLeft: '-2rem', marginTop: '6px' }}>
                        <div className="d-flex align-items-center">
                            <div className="d-block" style={{ width: '40%' }}>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                            </div>
                            <Progress color='secondary' value={ratePercents[5]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                        </div>
                        <div className="d-flex align-items-center">
                            <div className="d-block" style={{ width: '40%' }}>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                            </div>
                            <Progress color='secondary' value={ratePercents[4]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                        </div>
                        <div className="d-flex align-items-center">
                            <div className="d-block" style={{ width: '40%' }}>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                            </div>
                            <Progress color='secondary' value={ratePercents[3]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                        </div>
                        <div className="d-flex align-items-center">
                            <div className="d-block" style={{ width: '40%' }}>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                            </div>
                            <Progress color='secondary' value={ratePercents[2]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                        </div>
                        <div className="d-flex align-items-center">
                            <div className="d-block" style={{ width: '40%' }}>
                                <i className="fa fa-star text-muted float-right" style={{ fontSize: '12px' }}></i>
                            </div>
                            <Progress color='secondary' value={ratePercents[1]} className="ml-2" style={{ height: '8px', width: '60%' }}></Progress>
                        </div>

                        <div className="w-100 mt-1">
                            <div className="float-right">{rateCount} Penilaian</div>
                        </div>
                    </div>
                </Col>
                {write &&
                    <Col xs="12" className="px-0 d-flex align-items-center position-relative" style={{ borderTop: '1px solid' }}>
                        <div className="mr-1 font-lg">{hasRated ? 'Penilaian dari anda' : 'Beri penilaian anda'}</div>
                        {hasRated && <div className="position-absolute w-100 h-100" style={{ zIndex: 2 }}></div>}
                        <ReactStars
                            count={5}
                            onChange={(e) => postRating(e)}
                            size={30}
                            value={hasRated?.rate ?? 0}
                            edit={!hasRated}
                            // isHalf={true}
                            emptyIcon={<i className="fa fa-star"></i>}
                            halfIcon={<i className="fa fa-star-half-alt"></i>}
                            fullIcon={<i className="fa fa-star"></i>}
                            activeColor="#ffd700"
                        />
                    </Col>
                }
            </Row>
        </>
    )
})

export const RatingPreview = memo(({ data }) => {
    const [starKeyForce, setStarKeyForce] = useState(0)

    const rate1 = useMemo(() => data[1] ?? [], [data])
    const rate2 = useMemo(() => data[2] ?? [], [data])
    const rate3 = useMemo(() => data[3] ?? [], [data])
    const rate4 = useMemo(() => data[4] ?? [], [data])
    const rate5 = useMemo(() => data[5] ?? [], [data])

    const rateCount = useMemo(() => parseInt(rate5.length) + parseInt(rate4.length) + parseInt(rate3.length) + parseInt(rate2.length) + parseInt(rate1.length), [rate5, rate4, rate3, rate2, rate1])
    const rateAmount = useMemo(() => ((5 * rate5.length) + (4 * rate4.length) + (3 * rate3.length) + (2 * rate2.length) + (1 * rate1.length)) / rateCount, [rate5, rate4, rate3, rate2, rate1, rateCount])

    useEffect(() => {
        setStarKeyForce(prev => prev + 1)
    }, [rateAmount])

    return (
        <>
            <ReactStars
                count={5}
                size={16}
                value={rateAmount ? rateAmount : 0}
                edit={false}
                isHalf={true}
                emptyIcon={<i className="fa fa-star"></i>}
                halfIcon={<i className="fa fa-star-half-alt"></i>}
                fullIcon={<i className="fa fa-star"></i>}
                activeColor="#ffd700"
                key={starKeyForce}
            />
        </>
    )
})
