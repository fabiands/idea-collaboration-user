import React, { useEffect, useState } from 'react'
import { Input, Spinner } from 'reactstrap';
import { Link } from 'react-router-dom'
import noProject from '../../../../assets/img/no-project.png'
import request from '../../../../utils/request'
import { badgeStatus } from '../ProjectCard'

function Search() {
    const [filter, setFilter] = useState('')
    const [loading, setLoading] = useState(true)
    const onErrorImage = (e) => {
        e.target.src = noProject;
        e.target.onerror = null;
    }
    const [option, setOption] = useState([])

    useEffect(() => {
        request.get('v1/projects?verified=verified&search=' + filter ?? 0).then(res => {
            setOption(res.data.data);
            setLoading(false)
        })
    }, [filter])

    const handleSearchChange = (e) => {
        setFilter(e.target.value)
    }

    if (loading) {
        return (
            <div className="text-center" style={{ position: 'absolute', width: '100%', height: '100%', zIndex: '99', backgroundColor: 'rgba(255,255,255, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        background: "rgba(255,255,255, 0.5)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Spinner style={{ width: 48, height: 48 }} />
                </div>
            </div>
        )
    }

    return (
        <div className="px-2 py-3">
            <Input type="text" onChange={handleSearchChange} value={filter} placeholder="Cari nama proyek..." />
            {filter && option?.map((p, i) => (
                <Link className="d-flex mt-3 text-dark" to={`/project/${p.code}`} key={i}>
                    <div className="d-flex align-items-center justify-content-center">
                        <img src={p?.media[0]?.storage} width={60} height={60} alt="project" className="profile-photo-review rounded" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                    </div>
                    <div className="d-flex justify-content-between w-100">
                        <div className="d-flex align-items-center">
                            <div className="px-3">
                                <b>{p.title}</b><br />
                                <span>
                                    {badgeStatus(p?.status)}
                                </span>
                            </div>
                        </div>
                    </div>
                </Link>
            ))}
        </div>
    )
}

export default Search