import React, { useEffect, useMemo, useRef, useState } from 'react'
import { Link, Redirect, useLocation, useRouteMatch } from 'react-router-dom';
// import { toast } from 'react-toastify';
import { Spinner, Nav, NavItem, NavLink, Row, Col, Card, CardHeader, CardBody, TabContent, TabPane, Button, Input } from 'reactstrap'
import useSWR from 'swr';
import DesignSprint from './Sprint/DesignSprint';
import profilePhotoNotFound from '../../../assets/img/no-photo.png';
import TeamDetail from './TeamDetail';
import useSocket from '../../../hooks/useSocket';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';
import { toast } from 'react-toastify';
import request from '../../../utils/request';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useAuthUser } from '../../../store';

const socket = useSocket('/v1/sprint');
const tabs = {
    'sprint': 'Design Sprint',
    'myteam': 'Detail Team',
}

const tabsArray = Object.keys(tabs);

function TeamWrapper() {
    const user = useAuthUser()
    const inputFile = useRef(null)
    const matchRoute = useRouteMatch();
    const location = useLocation()
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];
    const search = new URLSearchParams(location.search);
    const { data, error: dataError } = useSWR('v1/teams/' + matchRoute.params.teamId.replaceAll("#myteam", ""), { refreshInterval: 1000000 });
    const loading = !data && !dataError;
    const getTeam = useMemo(() => data?.data?.data ?? [], [data]);
    const [trueMember, setTrueMember] = useState(true);

    const { data: getMember, mutate: mutateMember, error: getMemberError } = useSWR('v1/teams/' + matchRoute.params.teamId.replaceAll("#myteam", "") + '/members', { refreshInterval: 1000000 });
    const dataMember = useMemo(() => getMember?.data?.data ?? [], [getMember]);

    const [teamName, setTeamName] = useState({
        edit: false,
        value: ''
    })
    const [photoTeam, setPhotoTeam] = useState({
        file: null,
        preview: null,
    })

    const onChangeFile = (e) => {
        e.preventDefault();
        const data = e.target.files[0]
        if (data.size > 5242880) {
            toast.error('Foto melebihi ukuran maksimal')
            return;
        }

        let formData = new FormData();
        if (data) formData.append('photo', data, data.name)

        request.put('v1/teams/' + matchRoute.params.teamId, formData)
            .then(() => {
                // toast.success('Berhasil mengubah foto tim');
                setPhotoTeam({ file: data, preview: URL.createObjectURL(data) })
            })
            .catch(err => {
                if (err.response?.status === 422) {
                    toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                    return;
                }
                else if (err.response?.status) {
                    toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                    return;
                }
                Promise.reject(err);
            })
    }

    useEffect(() => {
        setTeamName(state => ({ ...state, value: getTeam?.lead?.leadName }))
        //eslint-disable-next-line
    }, [getTeam?.lead?.leadName])

    useEffect(() => {
        if (dataMember?.length > 0) {
            const isMember = dataMember.find(e => e.user.id === user.id && e.status === 'approved' && getTeam?.status === 'approved');
            if (!isMember) {
                setTrueMember(false)
            }
        }
    }, [user, dataMember, getTeam])

    if (!trueMember) {
        return <Redirect to="/" />
    }

    const handleChangeTeamName = (e) => {
        const { value } = e.target
        setTeamName(state => ({ ...state, value: value }))
    }
    const handleBlurTeamName = (e) => {
        const { value } = e.target
        setTeamName(state => ({ ...state, edit: false }))
        if (value !== getTeam?.lead?.leadName) {
            request.put('v1/teams/' + matchRoute.params.teamId, { teamName: value })
                .then(() => {
                    // toast.success('Berhasil mengubah foto tim');
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                        return;
                    }
                    else if (err.response?.status) {
                        toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                        return;
                    }
                    Promise.reject(err);
                })
        }
    }

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    if (loading || !getMember || !getTeam || getMemberError || dataError) {
        return (
            <div className="text-center" style={{ position: 'absolute', width: '100%', height: '100%', zIndex: '99', backgroundColor: 'rgba(255,255,255, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        background: "rgba(255,255,255, 0.5)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Spinner style={{ width: 48, height: 48 }} />
                </div>
            </div>
        )
    }

    return (
        <Row>
            <Col xs="2" className="px-0 d-none d-lg-flex">
                <Card className="shadow-sm design-sprint border-0">
                    <CardHeader className="design-sprint-header border-bottom-0 text-center">
                        <div className="rounded-circle lead-photo mb-3 d-flex justify-content-center align-items-center">
                            {getTeam?.lead?.leadPhoto ?
                                <img src={photoTeam?.preview ?? getTeam?.lead?.leadPhoto} alt="profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                :
                                <DefaultProfile init={getTeam?.lead?.leadName} size="100px" />
                            }
                            <input type='file' ref={inputFile} style={{ display: 'none' }} onChange={(e) => onChangeFile(e)} accept="image/png, image/gif, image/jpeg" />
                            <Button
                                className={`btn border-0 rounded-circle upload-file-button d-block ${(photoTeam?.preview ?? getTeam?.lead?.leadPhoto) && 'filled'}`}
                                style={{ width: '100px', height: '100px' }}
                                onClick={() => inputFile.current.click()}
                            >
                                <i className="fa fa-2x fa-camera" />
                                <br />
                                <div className="text-center d-none d-md-block">
                                    Upload
                                    <br />
                                    <small>Max. 5 MB</small>
                                </div>
                            </Button>
                        </div>
                        <div className="ml-2">
                            {teamName.edit ?
                                <Input type="text" value={teamName.value} onChange={handleChangeTeamName} onBlur={handleBlurTeamName} autoFocus />
                                :
                                <b className="font-lg" onClick={() => {
                                    setTeamName({ value: getTeam?.lead?.leadName, edit: true })
                                }} style={{ cursor: 'pointer' }}>
                                    Tim {teamName.value} <FontAwesomeIcon icon="edit" />
                                </b>
                            }
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Nav vertical className="tour-tabApplicantDetail">
                            {tabsArray.map(tab => (
                                <NavItem key={tab}>
                                    <NavLink tag={Link} active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                                        {tabs[tab]}
                                    </NavLink>
                                </NavItem>
                            ))}
                        </Nav>
                    </CardBody>
                </Card>
            </Col>
            <Col xs="12" lg="10" className="px-0">
                <TabContent activeTab={selectedTab} className="p-0">
                    <TabPane tabId="sprint" className="py-0">
                        <Row>
                            <Col sm="12">
                                <DesignSprint title={getTeam?.project?.title} socket={socket} project={getTeam.project} members={dataMember?.filter(member => member.status === 'approved')} leadId={getTeam?.lead?.leadId} />
                            </Col>
                        </Row>
                    </TabPane>

                    <TabPane tabId="myteam" className="py-0">
                        <Row>
                            <Col sm="12">
                                <TeamDetail socket={socket} leadId={getTeam?.lead?.leadId} data={dataMember} loading={!dataMember} status={search.get('status') ?? 'approved'} teamId={matchRoute.params.teamId.replaceAll("#myteam", "")} mutate={() => mutateMember()} />
                            </Col>
                        </Row>
                    </TabPane>
                </TabContent>
            </Col>
        </Row>
    )
}

export default TeamWrapper;