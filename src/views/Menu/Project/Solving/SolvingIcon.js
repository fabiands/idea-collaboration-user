import React from 'react'
import { ArcherContainer, ArcherElement } from 'react-archer'
import { Col, Row } from 'reactstrap'

function SolvingIcon({message, team, finish, mb}) {

    return (
        <div className={`mb-3 mb-md-${mb} mx-auto`} style={{width:'80%'}}>
            <ArcherContainer>
                <Row>
                    <Col className="text-center">
                        <ArcherElement
                            id="message"
                            relations={[
                                {
                                    targetId: 'team',
                                    targetAnchor: 'middle',
                                    sourceAnchor: 'middle',
                                    style: { strokeColor: '#4941E3', strokeWidth: 1, endMarker:false },
                                },
                            ]}
                            >
                            <div className={`mx-auto round-100 border-0 text-center d-flex justify-content-center align-items-center`} style={{ backgroundColor: message ? '#372974' : '#fff', width: '60px', height: '60px' }}>
                                <img src={require(`../../../../assets/img/solving/message-${message ? `active` : `nonactive`}.png`)} width={30} height={30} alt="message" style={{objectFit:'cover'}} />
                            </div>
                        </ArcherElement>
                        <span className="mt-2">
                            <br />
                            Masukkan Deskripsi Ide
                        </span>
                    </Col>
                    <Col className="text-center">
                        <ArcherElement
                            id="team"
                            relations={[
                                {
                                    targetId: 'finish',
                                    targetAnchor: 'middle',
                                    sourceAnchor: 'middle',
                                    style: { strokeColor: '#4941E3', strokeWidth: 1, endMarker:false },
                                },
                            ]}
                        >
                            <div className={`mx-auto round-100 border-0 text-center d-flex justify-content-center align-items-center`} style={{ backgroundColor: team ? '#372974' : '#fff', width: '60px', height: '60px' }}>
                                <img src={require(`../../../../assets/img/solving/team-${team ? `active` : `nonactive`}.png`)} width={30} height={30} alt="team" style={{objectFit:'cover'}} />
                            </div>
                        </ArcherElement>
                        <span className="mt-2">
                            <br />
                            Pilih Pembentukan Tim
                        </span>
                    </Col>
                    <Col className="text-center">
                        <ArcherElement id="finish">
                            <div className={`mx-auto round-100 border-0 text-center d-flex justify-content-center align-items-center`} style={{ backgroundColor: finish ? '#372974' : '#fff', width: '60px', height: '60px' }}>
                                <img src={require(`../../../../assets/img/solving/finish-${finish ? `active` : `nonactive`}.png`)} width={30} height={30} alt="finish" style={{objectFit:'cover'}} />
                            </div>
                        </ArcherElement>
                        <span className="mt-2">
                            <br />
                            Selesai
                        </span>
                    </Col>
                </Row>
            </ArcherContainer>
        </div>
    )
}

export default SolvingIcon