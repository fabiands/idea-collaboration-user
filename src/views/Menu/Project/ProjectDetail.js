import React, { useEffect, useState, useMemo, memo, useCallback } from 'react'
import {
    Card, CardBody, CardHeader, CardFooter,
    Carousel, CarouselControl, CarouselItem,
    Col, Row,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Spinner, Button, Input, Badge, Tooltip, UncontrolledPopover, PopoverBody,
    Nav, NavItem, NavLink
} from 'reactstrap'
import * as moment from 'moment'
import request from '../../../utils/request';
import { useAuthUser } from '../../../store';
import { Link, Redirect, useHistory, useRouteMatch } from 'react-router-dom';
import { toast } from 'react-toastify';
import noProject from '../../../assets/img/no-project.png';
import profilePhotoNotFound from '../../../assets/img/no-photo.png';
import useSWR from 'swr';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useFormik } from 'formik';
import SelectMap from './Create/SelectMap';
import TextareaAutosize from 'react-textarea-autosize';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';
import { useMediaQuery } from 'react-responsive';
import ModalCopyLink from '../../../components/ModalCopyLink';
import ReactStars from "react-rating-stars-component";
import FindHighestValue from '../../../utils/FindHighestValue';
import ReactPlayer from 'react-player/lazy'

function ProjectDetail() {
    const isSmallSize = useMediaQuery({ query: '(max-width: 768px)' });
    const matchRoute = useRouteMatch();
    const user = useAuthUser();
    const history = useHistory()
    const [like, setLike] = useState(false)
    const [unlike, setUnlike] = useState(false)
    const [hasAction, setHasAction] = useState(false)
    const [up, setUp] = useState(0)
    const [down, setDown] = useState(0)
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
    const [submitLoadPublish, setSubmitLoadPublish] = useState(false);
    const [selectLocation, setSelectLocation] = useState(false);
    const [loadingLocation, setLoadingLocation] = useState(true);
    const [isEditing, setIsEditing] = useState(false);
    const [modalDelete, setModalDelete] = useState(null)
    const [modalCopyLink, setModalCopyLink] = useState({
        link: '',
        isOpen: false,
    })
    const [isVoting, setIsVoting] = useState(false)
    const [isUser, setIsUser] = useState(true)
    const [videoPlayed, setVideoplayed] = useState({
        id: null
    })

    const handlePlayVideo = useCallback((id) => {
        setVideoplayed((state) => ({ ...state, id: id }))
    }, [])

    const handlePauseVideo = useCallback(() => {
        setVideoplayed((state) => ({ ...state, id: null }))
    }, [])

    const { data: dataDetail, error: dataDetailError, mutate: dataDetailMutate } = useSWR('v1/projects/' + matchRoute.params.code, { refreshInterval: 15000 });
    const { data: dataUserListedSWR, error: dataUserListedError, mutate: dataUserListedMutate } = useSWR('v1/projects/' + matchRoute.params.code + '/users', { refreshInterval: 15000 });
    const { data: dataTeamsSWR, error: dataTeamsError, mutate: dataTeamsMutate } = useSWR('v1/projects/' + matchRoute.params.code + '/teams', { refreshInterval: 15000 });

    const { values, touched, errors, isSubmitting, ...formik } = useFormik({
        initialValues: {
            title: '',
            description: '',
            locationName: '',
            locationLatitude: '',
            locationLongitude: '',
            locationCity: '',
            locationProvince: '',
        },
        // validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
            setSubmitLoadPublish(true);
            setIsEditing(false)
            request.put('v1/projects/' + matchRoute.params.code, values)
                .then(() => {
                    toast.success('Berhasil');
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                        setErrors(err.response.data.errors);
                        return;
                    }
                    else if (err.response?.status) {
                        toast.error("Terjadi kesalahan, silahkan coba lagi");
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => {
                    setSubmitLoadPublish(false);
                    setSubmitting(false);
                });
        }
    });

    const toggleModalDelete = useCallback((e) => {
        setModalDelete(null)
    }, [])

    const toggleModalCopyLink = (link) => {
        setModalCopyLink({ link: link, isOpen: !modalCopyLink.isOpen });
    }

    const handleDelete = useCallback((e) => {
        if (e) {
            request.delete('v1/projects/' + matchRoute.params.code)
                .then(() => {
                    toast.success('Berhasil Hapus');
                    history.goBack()
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        toast.error("Terjadi Kesalahan Pengisian, silahkan cek data yang anda isikan");
                        return;
                    }
                    else if (err.response?.status) {
                        toast.error("Terjadi kesalahan, silahkan coba lagi");
                        return;
                    }
                    Promise.reject(err);
                })
        }
        setModalDelete(false)
    }, [matchRoute, history])

    const data = useMemo(() => {
        setUp(dataDetail?.data?.data?.votes?.filter(item => item.type === 'up').length)
        setDown(dataDetail?.data?.data?.votes?.filter(item => item.type === 'down').length)

        if (dataDetail?.data?.data) {
            formik.setFieldValue('title', dataDetail?.data?.data?.title)
            formik.setFieldValue('description', dataDetail?.data?.data?.description)
        }

        return dataDetail?.data?.data ?? []
        // eslint-disable-next-line
    }, [dataDetail]);

    const dataUserListed = useMemo(() => dataUserListedSWR?.data?.data ?? [], [dataUserListedSWR]);
    const dataTeams = useMemo(() => dataTeamsSWR?.data?.data ?? [], [dataTeamsSWR]);

    const mutateAll = () => {
        dataDetailMutate()
        dataUserListedMutate()
        dataTeamsMutate()
    }

    // const goToIndex = (newIndex) => {
    //     if (animating) return;
    //     setActiveIndex(newIndex);
    // }
    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === data?.media?.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }
    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? data?.media?.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const doLike = (code) => {
        setIsVoting(true)
        if (like) {
            setLike(false)
            request.post(`v1/projects/${code}/vote`, { type: 'up' })
                .then(() => setUp(up - 1))
                .catch(() => setLike(true))
                .finally(() => {
                    setIsVoting(false)
                    setHasAction(false)
                })
        }
        if (!like) {
            setLike(true)
            setUnlike(false)
            request.post(`v1/projects/${code}/vote`, { type: 'up' })
                .then(() => {
                    if (hasAction) {
                        setUp(up + 1)
                        setDown(down - 1)
                    }
                    setUp(up + 1)
                })
                .catch(() => setLike(false))
                .finally(() => {
                    setIsVoting(false)
                    setHasAction(true)
                })
        }
    }

    const doUnLike = (code) => {
        setIsVoting(true)
        if (unlike) {
            setUnlike(false)
            request.post(`v1/projects/${code}/vote`, { type: 'down' })
                .then(() => setDown(down - 1))
                .catch(() => setUnlike(true))
                .finally(() => {
                    setIsVoting(false)
                    setHasAction(false)
                })
        }
        if (!unlike) {
            setLike(false)
            setUnlike(true)
            request.post(`v1/projects/${code}/vote`, { type: 'down' })
                .then(() => {
                    if (hasAction) {
                        setUp(up - 1)
                        setDown(down + 1)
                    }
                    setDown(down + 1)
                })
                .catch(() => setUnlike(false))
                .finally(() => {
                    setIsVoting(false)
                    setHasAction(true)
                })
        }
    }

    const actionUp = useMemo(() => data?.votes?.find(item => item.userId === user.id && item.type === 'up'), [data, user])
    const actionDown = useMemo(() => data?.votes?.find(item => item.userId === user.id && item.type === 'down'), [data, user])

    useEffect(() => {
        if (actionUp || actionDown) {
            setHasAction(true)
        }
        if (actionUp) {
            setLike(true)
        }
        if (actionDown) {
            setUnlike(true)
        }
    }, [actionUp, actionDown])

    useEffect(() => {
        if (dataDetail) {
            if (dataDetail?.data?.data?.user?.id !== user.id && data.status === 'verification') {
                setIsUser(false)
            }
        }
    }, [user, dataDetail, data.status])

    const onErrorProject = (e) => {
        e.target.src = noProject;
        e.target.onerror = null;
    }

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    const toggleLocation = () => setSelectLocation(!selectLocation);

    const handleLocation = useCallback((location) => {
        formik.setFieldValue('locationName', location.address)
        formik.setFieldValue('locationLatitude', location.latitude)
        formik.setFieldValue('locationLongitude', location.longitude)
        formik.setFieldValue('locationCity', location.city)
        formik.setFieldValue('locationProvince', location.province)
    }, [formik])

    if (!dataDetail || !dataTeamsSWR || !dataUserListedSWR || dataDetailError || dataTeamsError || dataUserListedError) {
        return (
            <div className="text-center" style={{ position: 'absolute', width: '100%', height: '100%', zIndex: '99', backgroundColor: 'rgba(255,255,255, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        background: "rgba(255,255,255, 0.5)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Spinner style={{ width: 48, height: 48 }} />
                </div>
            </div>
        )
    }

    if (!isUser) {
        return <Redirect to="/" />
    }

    return (
        <Row className="p-0 mb-5 mb-lg-0">
            <Col xs="12" md={data?.verified === 'pending' ? '12' : '7'}>
                <Card className="border-0 shadow-sm project-card card-detail-project" style={{ borderRadius: '5px' }}>
                    <CardHeader className="bg-white border-bottom-0 px-4 pb-0" style={{ position: 'relative' }}>
                        <div className="d-flex mt-3">
                            <div className="text-left pr-md-0">
                                {data?.user?.photo ?
                                    <img src={data?.user?.photo} alt="profile" className="profile-photo-project rounded-circle" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                    :
                                    <DefaultProfile init={data?.user?.name} size="50px" />
                                }
                            </div>
                            <div className="text-left px-3 w-75">
                                <b>{data.user?.name}</b><br />
                                {isEditing ?
                                    <Row>
                                        <Col xs="11">
                                            <Input type="text" placeholder="Lokasi" className="input-search" name="locationName" id="locationName"
                                                value={values.locationName}
                                                onChange={formik.handleChange}
                                                onBlur={formik.handleBlur} />
                                        </Col>
                                        <Col xs="1" className="p-0 d-flex justify-content-center">
                                            <Button color="primary" onClick={() => setSelectLocation(true)}>
                                                <FontAwesomeIcon icon="map-marker-alt" className="mx-auto" />
                                            </Button>
                                        </Col>
                                    </Row>
                                    :
                                    <a target="_blank" rel="noopener noreferrer" href={`http://www.google.com/maps/place/${data.locationLatitude},${data.locationLongitude}`}>
                                        <div className="text-secondary">
                                            {data.locationName}
                                        </div>
                                    </a>
                                }
                            </div>
                            <div className="text-dark-secondary" style={{ position: 'absolute', top: '30px', right: '20px' }}>
                                {
                                    data.verified === 'verified' ?
                                        badgeStatus(data.status)
                                        : data.verified === 'rejected' ?
                                            <Badge color="danger" pill className="text-capitalize text-light">
                                                Ditolak
                                            </Badge>
                                            : dataDetail?.data?.data?.user?.id !== user.id ?
                                                null
                                                : !isEditing ?
                                                    <>
                                                        <FontAwesomeIcon icon="ellipsis-h" id="project-detail-tooltip" style={{ cursor: 'pointer' }} className="text-muted" />
                                                        <UncontrolledPopover trigger="legacy" placement="bottom" target={`project-detail-tooltip`}>
                                                            <PopoverBody>
                                                                <Nav vertical>
                                                                    <NavItem>
                                                                        <NavLink style={{ borderRadius: '5px', cursor: 'pointer' }} onClick={() => setIsEditing(true)}>
                                                                            <FontAwesomeIcon icon="edit" /> Ubah
                                                                        </NavLink>
                                                                    </NavItem>
                                                                    <NavItem>
                                                                        <NavLink style={{ borderRadius: '5px', cursor: 'pointer' }} onClick={() => setModalDelete(true)}>
                                                                            <FontAwesomeIcon icon="trash" /> Hapus
                                                                        </NavLink>
                                                                    </NavItem>
                                                                </Nav>
                                                            </PopoverBody>
                                                        </UncontrolledPopover>
                                                    </>
                                                    :
                                                    <Button
                                                        className="mr-2 mt-3 d-none d-lg-block float-right"
                                                        onClick={formik.handleSubmit}
                                                        color="netis-primary"
                                                        disabled={submitLoadPublish}
                                                    >
                                                        {submitLoadPublish ? <><Spinner color="light" size="sm" /> Loading...</> : 'Simpan'}
                                                    </Button>
                                }
                            </div>
                        </div>
                    </CardHeader>
                    <CardBody style={{ borderTop: '1px solid #c8ced3' }} className="card-project-small text-left px-0 border-top-0">
                        <div className="desc-card-project px-4">
                            <b style={{ fontSize: '16px' }}>
                                {!isEditing ?
                                    <TextareaAutosize
                                        className="form-control card-detail-title is-filled"
                                        style={{ cursor: 'default' }}
                                        disabled={!isEditing}
                                        value={values.title}
                                    />
                                    :
                                    <TextareaAutosize
                                        rows="3"
                                        name="title" id="title"
                                        className="form-control mb-2"
                                        disabled={!isEditing}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={values.title}
                                        placeholder="Masukkan judul disini..."
                                    />
                                }
                            </b>
                            <p style={{ fontSize: '13px' }}>
                                {!isEditing ?
                                    <TextareaAutosize
                                        className="form-control card-detail-desc is-filled"
                                        style={{ cursor: 'default' }}
                                        disabled={!isEditing}
                                        value={values.description}
                                    />
                                    :
                                    <TextareaAutosize
                                        rows="3"
                                        name="description" id="description"
                                        className="form-control"
                                        disabled={!isEditing}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        value={values.description}
                                        placeholder="Masukkan deskripsi disini..."
                                    />
                                }
                            </p>
                            {isEditing &&
                                <Button
                                    className="mb-3 d-block d-lg-none float-right"
                                    onClick={formik.handleSubmit}
                                    color="netis-primary"
                                    disabled={submitLoadPublish}
                                    style={{ zIndex: 100 }}
                                >
                                    {submitLoadPublish ? <><Spinner color="light" size="sm" /> Loading...</> : 'Simpan'}
                                </Button>
                            }
                        </div>
                        <Carousel
                            activeIndex={activeIndex}
                            next={next}
                            previous={previous}
                            // ride={false}
                            enableTouch={true}
                            interval={false}
                            className="carousel-post mt-5 mt-lg-0"
                        >
                            {data.media?.map((item, idx) => (
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={idx}
                                    className="py-auto"
                                >
                                    {item.mimeType.includes('video') ?
                                        <div className="position-relative w-100 h-100" onClick={() => videoPlayed.id !== item.id ? handlePlayVideo(item.id) : handlePauseVideo()} style={{ cursor: 'pointer' }}>
                                            {videoPlayed.id !== item.id &&
                                                <FontAwesomeIcon className="position-absolute" style={{ top: '46%', left: '48%' }} icon="play" size="4x" color="white" />
                                            }
                                            <ReactPlayer url={item.storage} playing={videoPlayed.id === item.id ? true : false} width="100%" height='100%' />
                                        </div>
                                        :
                                        <img src={item.storage} alt={'media ' + (idx + 1)} width="100%" height="100%" style={{ objectFit: 'contain' }} onError={(e) => onErrorProject(e)} />
                                    }
                                </CarouselItem>
                            ))}
                            {data.media?.length > 0 &&
                                <>
                                    {activeIndex !== 0 && <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />}
                                    {activeIndex !== data?.media?.length - 1 && <CarouselControl direction="next" directionText="Next" onClickHandler={next} />}
                                </>
                            }
                        </Carousel>
                        <Row className="button-card-project pt-3">
                            {data?.verified === 'verified' &&
                                <>
                                    <Col xs="4" lg="4">
                                        <Row className="vote-row">
                                            <Col xs="4" lg="5" className={`vote-up text-center d-flex align-items-center justify-content-center ${like ? `bg-success` : `border-dark-secondary`}`} onClick={() => { if (!isVoting) doLike(data.code) }}>
                                                <i className={`fa ${!isSmallSize && `fa-lg`} fa-arrow-up ${like ? `scale-click` : ``}`} />
                                                <b className="ml-1">{up}</b>
                                            </Col>
                                            <Col xs="4" lg="5" className={`vote-down text-center d-flex align-items-center justify-content-center ${unlike ? `bg-secondary` : `border-dark-secondary`}`} onClick={() => { if (!isVoting) doUnLike(data.code) }}>
                                                <i className={`fa ${!isSmallSize && `fa-lg`} fa-arrow-down ${unlike ? `scale-click` : ``}`} />
                                                <b className="ml-1">{down}</b>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs="8" lg="8" className="px-0">
                                        <Row>
                                            <Col xs="5" className="text-left px-0">
                                                <Button className="mr-1" onClick={() => toggleModalCopyLink(`https://appolo-dev.widyaskilloka.com/project/${data.code}`)} style={{ borderRadius: '10px', backgroundColor: '#FAFAFA', borderColor: '#FAFAFA', color: '#807F7F' }}>
                                                    <i className="fa fa-share-alt" style={{ fontSize: '18pt' }} />
                                                </Button>
                                            </Col>
                                            <Col xs="7" className={`text-right pl-0 text-nowrap`}>
                                                {data.status === 'registration' &&
                                                    <Link to={`/project/${data?.code}/solving`}>
                                                        <Button color="primary" style={{ borderRadius: '10px', backgroundColor: 'rgba(91, 191, 250, 1)', borderColor: 'rgba(91, 191, 250, 1)' }} disabled={(dataUserListed.find(item => item.id === user.id) ? true : false) || data.status !== 'registration'}>
                                                            Selesaikan Masalah
                                                        </Button>
                                                    </Link>
                                                }
                                            </Col>
                                        </Row>
                                    </Col>
                                </>
                            }
                            <Col xs="12" className="p-0 mt-3">
                                <span className="text-secondary">
                                    {data.verifiedAt && moment(data.verifiedAt).fromNow()}
                                </span>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
                <SelectMap toggle={toggleLocation} isOpen={selectLocation} current={{ latitude: data.locationLatitude, longitude: data.locationLongitude }} location={handleLocation} loadingLocation={loadingLocation} setLoadingLocation={(e) => setLoadingLocation(e)} />
                <ModalDelete isOpen={modalDelete} toggle={toggleModalDelete} data={modalDelete} onDeleteCard={handleDelete} />
            </Col>
            {data?.verified !== 'pending' &&
                <Col xs="12" md="5">
                    <Row>
                        {data?.verified === 'verified' ?
                            <>
                                <Col xs="12">
                                    <TeamRegistered data={dataTeams} project={data} userListed={dataUserListed} mutate={mutateAll} isSmallSize={isSmallSize} />
                                </Col>
                                <Col xs={12}>
                                    <CommentProject data={data} onErrorImage={onErrorImage} mutate={() => dataDetailMutate()} />
                                </Col>
                            </>
                            :
                            <Col xs="12">
                                <Card className="border-0 shadow-sm" style={{ borderRadius: '5px' }}>
                                    <CardHeader className="bg-white border-bottom-0">
                                        <h5 className="mb-1 font-weight-bolder">Alasan Penolakan</h5>
                                    </CardHeader>
                                    <CardBody style={{ borderTop: '1px solid #c8ced3', height: '20vh' }} className="text-left border-top-0 py-1">
                                        <h6>{data?.verifiedMessage ?? "-"}</h6>
                                    </CardBody>
                                </Card>
                            </Col>
                        }
                    </Row>
                </Col>
            }
            <ModalCopyLink data={modalCopyLink} toggle={() => toggleModalCopyLink('')} />
        </Row >
    )
}


const ModalDelete = ({ isOpen, toggle, data, onDeleteCard }) => {
    const handleToggle = () => {
        toggle()
    }

    return (
        <Modal isOpen={!!isOpen} toggle={() => handleToggle()} size="sm" centered>
            <ModalHeader>
                Menghapus proyek
            </ModalHeader>
            <ModalBody>
                Apa anda yakin ingin menghapus proyek ini ?
            </ModalBody>
            <ModalFooter>
                <Button className="mr-2" color="netis-secondary" onClick={() => handleToggle()}>
                    Batal
                </Button>
                <Button color="netis-danger" onClick={() => {
                    onDeleteCard(true)
                    toggle()
                }}>
                    Hapus
                </Button>
            </ModalFooter>
        </Modal>
    )
}

const TeamRegistered = ({ data, project, userListed, mutate, isSmallSize, }) => {
    const user = useAuthUser();
    const approvedTeam = data?.filter(d => d.status === 'approved')
    const choosenTeam = useMemo(() => approvedTeam?.every(d => d.score !== null) && FindHighestValue(approvedTeam.filter(d => d.status === 'approved'), 'score', 1), [approvedTeam])
    const [modal, setModal] = useState(false)
    const [modalData, setModalData] = useState(null)

    const toggle = (e) => {
        setModal(false)
    }

    return (
        <Card className="border-0 shadow-sm" style={{ borderRadius: '5px' }}>
            <CardHeader className="bg-white border-bottom-0">
                <h5 className="mb-2 font-weight-bolder">Daftar Tim yang telah disetujui</h5>
            </CardHeader>
            <CardBody style={{ borderTop: '1px solid #c8ced3', maxHeight: '45vh', overflowY: 'scroll' }} className="text-left border-top-0 py-1">
                {data.find(item => item.status === 'approved') ?
                    <>
                        {data.filter(e => e.status === 'approved').map((item, idx) => (
                            <Card className={`card-team border-0 ${choosenTeam[0]?.id === item?.id && project?.status === 'finish' && 'ribbons'}`} data-label="Terpilih" style={{ borderRadius: '5px' }} key={idx}>
                                <div className="card-container">
                                    <CardBody className="pb-0 px-3">
                                        <Row className="card-team-info">
                                            <Col xs="12" className="d-flex justify-content-between align-items-center">
                                                <b>Tim {item.lead.leadName}</b>
                                            </Col>
                                            <Col xs="12" className="mb-3 d-flex justify-content-between align-items-end">
                                                <div className="d-flex flex-column flex-lg-fill float-left mb-7">
                                                    <span className="text-muted">Members</span>
                                                    <div className="symbol-group symbol-hover">
                                                        {item.members.filter(m => m.status === 'approved').map((member, k) => (
                                                            k >= 5 ? null :
                                                                <MemberItem member={member} key={k} />
                                                        ))}
                                                        {item.members.length > 5 &&
                                                            <div className="symbol symbol-30 symbol-circle symbol-light">
                                                                <span className="symbol-label font-weight-bold">{item.members.length - 5}+</span>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                                {item.score &&
                                                    <div>
                                                        <span className="text-muted">Nilai</span>
                                                        <ReactStars
                                                            count={5}
                                                            size={20}
                                                            value={item.score ?? 0}
                                                            edit={false}
                                                            // isHalf={true}
                                                            emptyIcon={<i className="fa fa-star"></i>}
                                                            halfIcon={<i className="fa fa-star-half-alt"></i>}
                                                            fullIcon={<i className="fa fa-star"></i>}
                                                            activeColor="#ffd700"
                                                        />
                                                    </div>
                                                }
                                            </Col>
                                            <Col xs="12" className="card-team-action p-0">
                                                {(item.members.find(m => m.id === user.id && m.status === 'approved') || item.lead.leadId === user.id) &&
                                                    <Link
                                                        key={idx}
                                                        to={{
                                                            pathname: `/project/${item.project.code}/team/${item.id}${isSmallSize ? '#myteam' : ''}`,
                                                            // search: `?team=${item.lead.leadId}`,
                                                            state: { team: item.lead.leadName }
                                                        }}
                                                    >
                                                        <Button className="w-100" size="lg" color="netis-color">
                                                            Lihat team saya
                                                        </Button>
                                                    </Link>
                                                }
                                                {
                                                    !userListed.find(item => item.id === user.id) ? (
                                                        project.status === 'registration' ?
                                                            <Button className="w-100" size="md" color="netis-success" onClick={() => {
                                                                setModalData(item)
                                                                setModal(true)
                                                            }}>
                                                                Gabung team
                                                            </Button>
                                                            :
                                                            null
                                                    )
                                                        :
                                                        item.members.find(m => m.id === user.id && m.status === 'pending') &&
                                                        <Button className="w-100 text-light" size="md" color="warning">
                                                            Menunggu Approval dari ketua tim
                                                        </Button>
                                                }
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </div>
                            </Card>
                        ))}
                    </>
                    :
                    <div>Belum ada Tim yang disetujui</div>
                }
                <ModalJoinTeam data={modalData} isOpen={modal} toggle={(e) => toggle(e)} mutate={() => mutate()} />
            </CardBody>
            <CardFooter className="border-top-0 bg-white"></CardFooter>
        </Card >
    )
}

export const MemberItem = memo(({ member, project }) => {
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
    const onErrorPhotoMember = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    return (
        <>
            <div className="symbol symbol-30 symbol-circle"
                id={`a${member?.id}-${project?.code.slice(0, 5)}`}
            >
                {member?.photo ?
                    <img alt="Pic" src={member?.photo} onError={(e) => onErrorPhotoMember(e)} />
                    :
                    <DefaultProfile init={member?.fullName} size="30px" />
                }
            </div>
            <Tooltip placement="bottom" isOpen={tooltipOpen}
                target={`a${member?.id}-${project?.code.slice(0, 5)}`}
                toggle={toggleTooltip}
            >
                {member.fullName}
            </Tooltip>
        </>
    );
});

const CommentProject = ({ data, onErrorImage, mutate }) => {
    const matchRoute = useRouteMatch();
    const user = useAuthUser();
    const [hasComment, setHasComment] = useState(false)
    const [value, setValue] = useState("")
    const [submitting, setSubmitting] = useState(false)
    const notNull = value.replace(/\s/g, "")
    // const [deletingId, setDeletingId] = useState(null)
    // const [deleting, setDeleting] = useState(false)
    // const [dataComment, setDataComment] = useState(data)

    const doComment = () => {
        setSubmitting(true)
        request.post(`v1/projects/${matchRoute.params.code}/comment`, { comment: value })
            .then((res) => {
                // console.log(res)
                if (res.status === 200) {
                    mutate()
                    setHasComment(!hasComment)
                    setValue("")
                    toast.success('Berhasil memberi komentar')
                }
            })
            .catch((err) => {
                console.log(err)
                toast.error('Gagal memberi komentar')
                return;
            })
            .finally(() => setSubmitting(false))
    }

    // const deleteComment = () => {
    //     setDeleting(true)
    //     request.delete(`v1/projects/${matchRoute.params.code}/comment/${deletingId}`)
    //         .then(() => {
    //             setDataComment(dataComment.filter(item => item.userId !== deletingId))
    //             setDeletingId(null)
    //         })
    //         .catch(() => toast.error('Gagal menghapus komentar'))
    //         .finally(() => setDeleting(false))
    // }


    return (
        <>
            <Card className="border-0 shadow-sm" style={{ borderRadius: '5px' }}>
                <CardHeader className="bg-white border-bottom-0">
                    <h5 className="mb-2 font-weight-bolder">Komentar</h5>
                </CardHeader>
                <CardBody style={{ borderTop: '1px solid #c8ced3', maxHeight: '40vh', overflowY: 'scroll' }} className="text-left border-top-0 py-0">
                    {data?.comments?.length > 0 && data?.comments?.map((item, idx) => (
                        <Row key={idx} className="pl-0">
                            <Col xs="2" className="d-flex justify-content-center align-items-center px-0">
                                {item.userPhoto ?
                                    <img src={item.userPhoto} alt="profile" className="rounded-circle" width={35} height={35} onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                    :
                                    <DefaultProfile init={item.userFullName} size="35px" />
                                }
                            </Col>
                            <Col xs="10" className="pl-0 m-auto">
                                <Card style={{ borderRadius: "15px" }} className="bg-light m-0 my-2">
                                    <CardBody className="py-2">
                                        {/* {item.userId === user.id &&
                                        <Button
                                            style={{position:'absolute', top:'2px', right:'2px'}}
                                            className="btn-sm bg-transparent border-0"
                                            onClick={() => setDeletingId(item.userId)}
                                        >
                                            <i className="fa fa-trash text-secondary" />
                                        </Button>
                                    } */}
                                        <Row>
                                            <Col xs="10">
                                                <strong>{item.userFullName}</strong><br />
                                                <span>{item.comment}</span><br />
                                            </Col>
                                            <Col xs="12" className="text-right">
                                                <small className="text-secondary">{moment(item.createdAt).fromNow()}</small>
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                    ))}
                </CardBody>
                <CardFooter className="border-top-0 bg-white pt-1">
                    <Row className="mt-3">
                        <Col xs="2" className="d-flex justify-content-center align-items-center px-0">
                            {user.detail.photo ?
                                <img src={user.detail.photo} alt="profile" className="rounded-circle" width={35} height={35} onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                :
                                <DefaultProfile init={user.detail.fullName} size="35px" />
                            }
                        </Col>
                        <Col xs="10" className="pl-0 m-auto">
                            <TextareaAutosize
                                rows="3"
                                name="comment" id="comment"
                                style={{ borderRadius: "10px" }}
                                className="form-control"
                                placeholder="Tuliskan komentarmu..."
                                onChange={(e) => setValue(e.target.value)}
                                onKeyPress={(e) => {
                                    if (e.key === 'Enter') {
                                        setValue(e.target.value)
                                        e.target.blur()
                                    }
                                }}
                                value={value}
                            />
                        </Col>
                        <Col xs="12" className="pl-0 m-auto">
                            <Button
                                color="netis-color"
                                style={{ borderRadius: '10px' }}
                                disabled={!notNull}
                                className="mt-2 px-3 ml-auto float-right"
                                onClick={doComment}
                            >
                                {submitting ? <Spinner size="sm" className="my-auto" /> : "Submit"}
                            </Button>
                        </Col>
                    </Row>
                </CardFooter>
            </Card>
            {/* <Modal isOpen={deletingId ? true : false} toggle={() => setDeletingId(null)}>
            <ModalBody className="text-center align-items-center pb-0">
                    Apakah anda yakin ingin menghapus komentar ini ?<br />
            </ModalBody>
            <ModalFooter className="border-top-0 text-center pb-4 pt-2">
                <div className="text-center d-flex justify-content-between mx-auto mt-2" style={{ width: "50%" }}>
                    <Button onClick={deleteComment} disabled={deleting} color="netis-primary" className="mr-2 px-2 btn-sm" style={{ width: "100px", borderRadius:'10px'}}>
                        {deleting ? <Spinner size="sm" className="mx-auto" /> : "Hapus"}
                    </Button>
                    <Button color="danger" onClick={() => setDeletingId(null)} disabled={deleting} className="ml-2 px-2 btn-sm" style={{ width: "100px", borderRadius:'10px'}}>
                        Batal
                    </Button>
                </div>
            </ModalFooter>
        </Modal> */}
        </>
    )
}

const badgeStatus = (status) => {
    let statusText = ''
    let statusColor = ''

    switch (status) {
        case 'registration':
            statusText = 'Pembentukan Tim'
            statusColor = 'warning'
            break;
        case 'ideation':
            statusText = 'Ideasi Tim'
            statusColor = 'info'
            break;
        case 'finish':
            statusText = 'Final Ide Tim'
            statusColor = 'success'
            break;
        default:
            break;
    }

    return (
        <Badge color={statusColor} pill className="text-capitalize text-light">
            {statusText}
        </Badge>
    )
}

const ModalJoinTeam = ({ data, isOpen, toggle, mutate }) => {
    const [ide, setIde] = useState('')
    const [loading, setLoading] = useState(false)
    const handleToggle = () => {
        toggle(false)
    }


    const handleSubmit = () => {
        setLoading(true)
        request.post(`v1/projects/${data.project.code}/solve`, { type: 'exist', teamId: data.id, message: ide })
            .then(() => {
                toast.success('Permintaan Gabung Tim Terkirim. Tunggu Konfirmasi Leader')
                mutate()
                toggle(false)
            })
            .catch(() => toast.error('Gagal Bergabung, Silahkan coba lagi'))
            .finally(() => setLoading(false))
    }

    return (
        <Modal isOpen={isOpen} toggle={() => handleToggle()}>
            <ModalHeader toggle={() => handleToggle()}>Gabung team {data?.lead?.leadName}</ModalHeader>
            <ModalBody>
                <Row>
                    <Col xs="12" className="px-0">
                        <Input
                            name="comment"
                            id="comment"
                            type="textarea"
                            style={{ borderRadius: "5px" }}
                            placeholder="Tuliskan deskripsi idemu..."
                            rows={5}
                            value={ide}
                            onChange={(e) => setIde(e.target.value)}
                        />
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button className="mr-2" color="netis-secondary" onClick={() => handleToggle()}>
                    Batal
                </Button>
                <Button className="mr-2" color="netis-primary" disabled={!ide || loading} onClick={() => handleSubmit()}>
                    {loading ?
                        <><Spinner color="light" size="sm" /> Loading...</> :
                        "Kirim"
                    }
                </Button>
            </ModalFooter>
        </Modal>
    )
}

export default ProjectDetail