import React from 'react'
import { useMediaQuery } from 'react-responsive'
import { Link, useLocation } from 'react-router-dom'
import { Card, CardBody } from 'reactstrap'
import SolvingIcon from './SolvingIcon'

const desc = {
    new: 'Selamat! Ide Anda berhasil dikumpulkan! Silahkan menunggu persetujuan dari Admin',
    exist: 'Permintaan gabung Tim berhasil, silahkan menunggu persetujuan dari Ketua Tim'
}

function SolvingFinish(){
    const isSmallSize = useMediaQuery({ query: '(max-width: 768px)' });
    const location = useLocation()
    return(
        <div className="mx-auto solving-mobile-container">
            {!isSmallSize && <SolvingIcon message={false} team={false} finish={true} mb={3} />}
            <Card className="pt-2 card-finish">
                {isSmallSize && 
                    <span style={{position:'absolute', top:'6px', right:'8px'}} className="text-secondary">
                        Langkah 3 dari 3
                    </span>}
                <CardBody className="mx-auto d-flex flex-column justify-content-center align-items-center">
                    <i className="fa fa-check-circle text-success mb-3" style={{fontSize:'8em'}} />
                    <span className="mb-3 text-center span-finish">
                        {desc[location.state.type]}
                    </span>
                    <Link to="/" style={{color:'#372974'}}>
                        <i className="fa fa-arrow-left mr-3" /> Beranda
                    </Link>
                </CardBody>
            </Card>
        </div>
    )
}

export default SolvingFinish