import React, { useEffect, useState, useMemo, useRef } from 'react'
import { Card, CardBody, Col, Row, Spinner, Button, ListGroup, ListGroupItem, Badge, Input, Label } from 'reactstrap'
// import * as moment from 'moment'
// import ReactMarkdown from "react-markdown";
import request from '../../../../utils/request';
import { useAuthUser } from '../../../../store';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { useSolvingContext } from './SolvingContext';
import Select from 'react-select';
import { toast } from 'react-toastify';
import SolvingIcon from './SolvingIcon';
import { useMediaQuery } from 'react-responsive';
import noPhoto from "../../../../assets/img/no-photo.png"
import { DefaultProfile } from '../../../../components/Initial/DefaultProfile';
import photoNotFound from '../../../../assets/img/no-photo.png';

function SolvingTeam() {
    const isSmallSize = useMediaQuery({ query: '(max-width: 768px)' });
    const matchRoute = useRouteMatch();
    const history = useHistory()
    const authUser = useAuthUser();
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [dataUser, setDataUser] = useState([]);
    const [dataUserListed, setDataUserListed] = useState([]);
    const [solving, setSolving] = useSolvingContext()
    const [submitLoad, setSubmitLoad] = useState(false)

    const onErrorImage = (e) => {
        e.target.src = noPhoto;
        e.target.onerror = null;
    }

    if (!solving.message) {
        history.push("/project/" + matchRoute.params.code + "/solving")
    }

    useEffect(() => {
        request.get('v1/projects/' + matchRoute.params.code).then(res => {
            setData(res.data.data);
        }).finally(() => setLoading(false))
        setSolving(state => ({ ...state, name: `${authUser.detail.fullName}` }))
    }, [matchRoute, authUser, setSolving]);

    useEffect(() => {
        request.get('v1/projects/' + matchRoute.params.code + '/users').then(res => {
            const user = res.data.data.filter((u) => u.id !== authUser.id)
            setDataUserListed(user);
        }).finally(() => setLoading(false))

        request.get('v1/users?emailMasking=true').then(res => {
            const user = res.data.data.filter((u) => u.id !== authUser.id)
            setDataUser(user);
        }).finally(() => setLoading(false))
    }, [authUser, matchRoute]);

    const handleChooseType = (type) => {
        setSolving(state => ({ ...state, type: type }))
    }

    const options = useMemo(() => {
        const opt = []
        // eslint-disable-next-line
        dataUser.map((v, idx) => {
            const dataOptions = dataUserListed.find(u => u.id === v.id);
            if (!dataOptions)
                opt.push({
                    id: v.id,
                    value: v.detail.fullName,
                    label:
                        <div key={idx} className="my-1 d-flex align-items-center">
                            {v.detail.photo ?
                                <img src={v.detail.photo} width={25} height={25} alt="profile" className="project-photo-review rounded-circle mr-1" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                :
                                <DefaultProfile init={v.detail.fullName} size="40px" />
                            }
                            <div className="text-left ml-1">
                                <h6 className="font-weight-bold">{v.detail.fullName}</h6>
                                <span className="text-secondary">{v.email}</span>
                            </div>
                        </div>
                })
        })

        return opt
    }, [dataUser, dataUserListed])

    const submitForm = (e) => {
        setSubmitLoad(true)
        if (!solving.name) setSolving(state => ({ ...state, name: `${authUser.detail.fullName}` }))

        let formData = new FormData();
        formData.append('message', solving.message)
        formData.append('teamId', solving.teamId)
        formData.append('type', solving.type)
        solving.userId.map(u => formData.append('userId[]', u))
        formData.append('teamName', solving.name)
        if (solving.photo) formData.append('photo', solving.photo, solving.photo.name)

        request.post('v1/projects/' + matchRoute.params.code + '/solve', formData)
            .then(() => {
                toast.success('Berhasil menambahkan data');
                history.push({
                    pathname: `/project/${matchRoute.params.code}/solving/finish`,
                    state: { type: solving.type }
                })
            })
            .catch(err => {
                toast.error(err.response.data.message);
                return;
            })
            .finally(() => {
                setSubmitLoad(false);
            });
    }

    if (loading) {
        return (
            <div className="text-center" style={{ position: 'relative', width: '100%', height: '100%', zIndex: '99', backgroundColor: 'rgba(255,255,255, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        background: "rgba(255,255,255, 0.5)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Spinner style={{ width: 48, height: 48 }} />
                </div>
            </div>
        )
    }

    return (
        <div className="mx-auto solving-mobile-container">
            {!isSmallSize &&
                <SolvingIcon
                    message={false}
                    team={true}
                    finish={false}
                    mb={5}
                />
            }
            <Card className="pt-1" style={{ position: 'relative' }}>
                {!isSmallSize ?
                    <Button style={{ position: 'absolute', top: '8px', left: '6px' }} className="bg-transparent border-0" onClick={() => history.goBack()}>
                        <i className="fa fa-arrow-left mx-2" /> Kembali
                    </Button>
                    :
                    <span style={{ position: 'absolute', top: '6px', right: '8px' }} className="text-secondary">
                        Langkah 2 dari 3
                    </span>
                }
                <CardBody className="mx-auto solving-card-team d-flex justify-content-center align-items-center">
                    {!solving.type ?
                        isSmallSize ?
                            <Row className="text-left">
                                <Col xs="12" className="pb-3">
                                    <h5>Pilih Tim</h5>
                                    <span style={{ color: '#807F7F' }}>
                                        Beritahu kami, kamu ingin membuat tim
                                        atau bergabung dengan tim yang telah ada.
                                    </span>
                                </Col>
                                <Col xs="12" className="py-4 card-button" onClick={() => handleChooseType('new')}><i className="fa fa-lg fa-circle text-secondary mr-2" />Buat Tim</Col>
                                <Col xs="12" className="py-4 card-button" onClick={() => handleChooseType('exist')}><i className="fa fa-lg fa-circle text-secondary mr-2" />Gabung Tim</Col>
                            </Row>
                            :
                            <Row className="d-flex justify-content-center align-items-center">
                                <Col sm="7" md="10">
                                    <h4 className="mb-4">
                                        Beritahu kami, kamu ingin membuat tim
                                        atau bergabung dengan tim yang telah ada.
                                    </h4>
                                </Col>
                                <Col sm="7" md="10" className="px-md-5">
                                    <Button className="shadow-sm my-2" block color="netis-primary" style={{ height: 70 }} onClick={() => handleChooseType('new')}>
                                        Buat Tim
                                    </Button>
                                </Col>
                                <Col sm="7" md="10" className="px-md-5">
                                    <Button className="shadow-sm my-2" block color="light" style={{ height: 70 }} onClick={() => handleChooseType('exist')}>
                                        Gabung Team
                                    </Button>
                                </Col>
                            </Row>
                        : (
                            solving.type === 'new' ?
                                <CreateTeam clearType={handleChooseType} options={options} onChangeMember={(e) => setSolving(state => ({ ...state, userId: e }))} onSetPhoto={(e) => setSolving(state => ({ ...state, photo: e }))} onSetNameTeam={(e) => setSolving(state => ({ ...state, name: e }))} solving={solving} onSubmit={submitForm} submitLoad={submitLoad} />
                                :
                                <ChooseTeam clearType={handleChooseType} dataProject={data} onChangeTeam={(e) => setSolving(state => ({ ...state, teamId: e }))} solving={solving} onSubmit={submitForm} submitLoad={submitLoad} />
                        )
                    }
                </CardBody>
            </Card>
        </div>
    )
}

const CreateTeam = ({ clearType, options, onChangeMember, onSetPhoto, onSetNameTeam, solving, onSubmit, submitLoad }) => {
    const authUser = useAuthUser();
    const inputFile = useRef(null)
    const [photoTeam, setPhotoTeam] = useState({
        file: null,
        preview: null,
    })
    const handleChangeMember = (e) => {
        const opt = []
        if (e) {
            // eslint-disable-next-line
            e.map(v => {
                opt.push(v.id)
            })
        }
        onChangeMember(opt)
    }

    const onChangeFile = (e) => {
        e.preventDefault();
        const data = e.target.files[0]
        if (data.size > 5242880) {
            toast.error('Foto melebihi ukuran maksimal')
            return;
        }
        setPhotoTeam({ file: e.target.files[0], preview: URL.createObjectURL(e.target.files[0]) })
        onSetPhoto(e.target.files[0])
    }

    const onErrorImage = (e) => {
        e.target.src = photoNotFound;
        e.target.onerror = null;
    }

    return (
        <Row className="d-flex justify-content-center align-items-center">
            <Col sm="7">
                <div className="mb-5">
                    <h3>
                        Buat Tim
                    </h3>
                    <p className="text-muted">
                        Tambahkanlah orang untuk menjadi anggota tim dalam proyek ini.
                    </p>
                </div>
            </Col>
            <Col sm="7">
                <div className="rounded-circle mb-3 d-flex justify-content-center align-items-center m-auto" style={{ width: '200px', height: '200px' }}>
                    {photoTeam?.preview ? <img src={photoTeam?.preview} alt="team" className="rounded-circle" style={{ objectFit: 'cover', position: 'absolute', width: '200px', height: '200px' }} onError={(e) => onErrorImage(e)} />
                        :
                        <DefaultProfile init={authUser.detail.fullName} size="200px" className="position-absolute" />
                    }
                    <input type='file' ref={inputFile} style={{ display: 'none' }} onChange={(e) => onChangeFile(e)} accept="image/png, image/gif, image/jpeg" />
                    <Button
                        className={`btn border-0 rounded-circle upload-file-button d-block ${photoTeam?.preview && 'filled'}`}
                        onClick={() => inputFile.current.click()}
                    >
                        <i className="fa fa-2x fa-camera" />
                        <br />
                        <div className="text-center d-none d-md-block">
                            Upload
                            <br />
                            <small>Max. 5 MB</small>
                        </div>
                    </Button>
                </div>
            </Col>
            <Col sm="7" className="mb-3">
                <Label>Nama Tim</Label>
                <Input type="text" value={solving.name} onChange={(e) => onSetNameTeam(e.target.value)} onBlur={(e) => !e.target.value ? onSetNameTeam(`${authUser.detail.fullName}`) : e.target.value} />
            </Col>
            <Col sm="7">
                <Label>Anggota Tim</Label>
                <ListGroup flush>
                    <ListGroupItem>
                        <Row>
                            <Col xs="9" className="p-0">
                                {authUser.detail.fullName}
                            </Col>
                            <Col xs="3" className="p-0 d-flex justify-content-center align-items-center">
                                <Badge color="warning" className="text-white">Ketua</Badge>
                            </Col>
                        </Row>
                    </ListGroupItem>
                    <ListGroupItem>
                        <Row>
                            <Col xs="9" className="p-0">
                                <Select
                                    closeMenuOnSelect={false}
                                    options={options}
                                    isClearable
                                    isMulti
                                    onChange={(e) => handleChangeMember(e)}
                                    components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }} />
                            </Col>
                            <Col xs="3" className="p-0 d-flex justify-content-center align-items-center">
                                <Badge color="success" className="text-white">Anggota</Badge>
                            </Col>
                        </Row>
                    </ListGroupItem>
                </ListGroup>
            </Col>
            <Col sm="7" className="d-flex justify-content-center align-items-center mt-3">
                <Button color="white" onClick={() => clearType('')}>
                    Kembali
                </Button>
                <Button
                    type="submit"
                    color="netis-primary"
                    disabled={solving.userId.length < 2 ? true : submitLoad}
                    onClick={() => {
                        onSubmit(true)
                    }}
                >
                    {submitLoad ? <><Spinner color="light" size="sm" /> Loading...</> : 'Lanjutkan'}
                </Button>
            </Col>
        </Row>
    )
}

const ChooseTeam = ({ clearType, dataProject, onChangeTeam, solving, onSubmit, submitLoad }) => {
    const filteredTeam = dataProject?.teams?.filter(e => e.status === 'approved')
    const handleChangeTeam = (teamId) => {
        onChangeTeam(teamId)
    }
    return (
        <Row className="d-flex justify-content-center align-items-center">
            <Col sm="7">
                <div className={dataProject?.teams?.length < 1 ? 'mb-3' : 'mb-5'}>
                    <h3>
                        Gabung Tim
                    </h3>
                    <p className="text-muted">
                        Silahkan pilih dan bergabung dengan tim yang sudah ada.
                    </p>
                </div>
            </Col>
            <Col sm="7">
                {filteredTeam.length < 1 ?
                    <span className="text-secondary mb-2"><i>Belum ada Tim yang terdaftar untuk Proyek ini</i></span>
                    :
                    <ListGroup flush className="join-team">
                        {filteredTeam?.map((item, idx) => (
                            <ListGroupItem key={idx} active={solving?.teamId === item.id} tag="button" action onClick={() => handleChangeTeam(item.id)}>{item.leadName}</ListGroupItem>
                        ))
                        }
                    </ListGroup>
                }
            </Col>
            <Col sm="7" className="d-flex justify-content-center align-items-center mt-3">
                <Button color="white" onClick={() => clearType('')}>
                    Kembali
                </Button>
                <Button
                    type="submit"
                    color="netis-primary"
                    disabled={!solving.teamId ? true : submitLoad}
                    onClick={() => {
                        onSubmit(true)
                    }}
                >
                    {submitLoad ? <><Spinner color="light" size="sm" /> Loading...</> : 'Lanjutkan'}
                </Button>
            </Col>
        </Row>
    )
}

export default SolvingTeam