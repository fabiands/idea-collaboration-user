import React, { useCallback, useEffect, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { Button, Card, CardBody, Col, Row } from 'reactstrap'
import { badgeStatus } from '../Project/ProjectCard'
import projectNoImage from '../../../assets/img/no-project.png';

const noDataDesc = {
    pending: 'belum terverifikasi',
    verified: 'disetujui',
    rejected: 'ditolak'
}

function ProjectList({ data }) {
    const location = useLocation()
    const search = new URLSearchParams(location.search);
    const [filter, setFilter] = useState(search.get('show') === 'projects' ? 'rejected' : 'pending')
    const [projectData, setProjectData] = useState([])

    useEffect(() => {
        setProjectData(data?.filter(e => e.verified === filter) ?? [])
    }, [data, filter])
    const seePending = () => {
        setFilter('pending')
        setProjectData(data.filter(e => e.verified === 'pending'))
    }
    const seeVerif = () => {
        setFilter('verified')
        setProjectData(data.filter(e => e.verified === 'verified'))
    }
    const seeReject = useCallback(() => {
        setFilter('rejected')
        setProjectData(data.filter(e => e.verified === 'rejected'))
    }, [data])

    const onErrorImage = (e) => {
        e.target.src = projectNoImage;
        e.target.onerror = null;
    }

    return (
        <Row>
            <Col xs="12" className="d-flex justify-content-center mb-3">
                <Button onClick={seePending} color={filter === 'pending' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                    Belum Verifikasi
                </Button>
                <Button onClick={seeVerif} color={filter === 'verified' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                    Disetujui
                </Button>
                <Button onClick={seeReject} color={filter === 'rejected' ? 'netis-color' : 'secondary'} className="mx-2" style={{ borderRadius: '10px' }}>
                    Ditolak
                </Button>
            </Col>
            {projectData?.length < 1 ?
                <Col xs="12" className="pt-4 mx-auto text-center">
                    Belum ada Proyek yang {noDataDesc[filter]}
                </Col>
                : projectData?.map((item, idx) => (
                    <Col xs="12" md="6" key={idx} className={`p-2`}>
                        <Link to={`/project/${item.code}`} className="text-dark">
                            <Card className="card-team-review mb-1" style={{ borderRadius: '5px' }}>
                                <CardBody>
                                    <Row className="card-team-info">
                                        <Col xs="12" className="d-flex mb-2">
                                            <img src={item?.media[0]?.storage} alt="project" className="project-photo-review rounded" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                            <div>
                                                <h6 className="font-weight-bold ml-3">{item.title}</h6>
                                                {/* <h6 className="text-muted ml-3 mb-0">Tim {item.lead.leadName}</h6> */}
                                            </div>
                                        </Col>
                                        {item.verified === 'verified' ?
                                            <>
                                                <Col xs="6">
                                                    <div className="d-flex flex-column flex-lg-fill float-left">
                                                        <span className="text-muted text-left">Status Proyek</span>
                                                        <div className="d-flex align-items-center mt-2">
                                                            {badgeStatus(item.status)}
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col xs="6">
                                                    <div className="d-flex flex-column flex-lg-fill float-right pt-4">
                                                        <span className="text-secondary">
                                                            {item?.comments?.length}&nbsp;Komentar
                                                            &bull;
                                                            {item?.teams?.length}&nbsp;Tim
                                                        </span>
                                                    </div>
                                                </Col>
                                            </>
                                            : item.verified === 'rejected' ?
                                                <Col xs="12">
                                                    <div className="d-flex flex-column flex-lg-fill float-left">
                                                        <span className="text-muted text-left">
                                                            <i>Alasan Penolakan : {item?.verifiedMessage ?? "-"}</i>
                                                        </span>
                                                    </div>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>
                ))
            }
        </Row>
    )
}

export default ProjectList