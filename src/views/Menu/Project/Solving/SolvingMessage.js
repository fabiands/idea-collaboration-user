import React, { useCallback, useEffect, useState } from 'react'
import { Card, CardBody, CardHeader, Carousel, CarouselControl, CarouselIndicators, CarouselItem, Col, Row, Spinner, Button, Input } from 'reactstrap'
import ReactMarkdown from "react-markdown";
import request from '../../../../utils/request';
import { useAuthUser } from '../../../../store';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { useSolvingContext } from './SolvingContext';
import noProject from '../../../../assets/img/no-project.png';
import profilePhotoNotFound from '../../../../assets/img/no-photo.png';
import { DefaultProfile } from '../../../../components/Initial/DefaultProfile';
import SolvingIcon from './SolvingIcon';
import { useMediaQuery } from 'react-responsive';
import ReactPlayer from 'react-player/lazy'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function SolvingMessage() {
    const matchRoute = useRouteMatch();
    const history = useHistory();
    const user = useAuthUser();
    const [loading, setLoading] = useState(true);
    const isSmallSize = useMediaQuery({ query: '(max-width: 768px)' });
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);
    const [data, setData] = useState([]);
    const [message, setMessage] = useState('')

    const [, setSolving] = useSolvingContext()
    const [dataUserListed, setDataUserListed] = useState([]);
    const [videoPlayed, setVideoplayed] = useState({
        id: null
    })

    const handlePlayVideo = useCallback((id) => {
        setVideoplayed((state) => ({ ...state, id: id }))
    }, [])

    const handlePauseVideo = useCallback(() => {
        setVideoplayed((state) => ({ ...state, id: null }))
    }, [])

    useEffect(() => {
        const detailProject = request.get('v1/projects/' + matchRoute.params.code)
        const detailProjectUsers = request.get('v1/projects/' + matchRoute.params.code + '/users')
        Promise.all([detailProject, detailProjectUsers]).then(([detailProject, detailProjectUsers]) => {
            if (detailProject.data) {
                setData(detailProject.data.data);
            }
            if (detailProjectUsers.data) {
                setDataUserListed(detailProjectUsers.data.data);
            }
        }).finally(() => setLoading(false))
    }, [matchRoute]);

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }
    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === data?.media?.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    }
    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? data?.media?.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    }

    const handleChangeMessage = (e) => {
        setMessage(e.target.value)
    }

    const nextStep = () => {
        setSolving(state => ({ ...state, message: message }))
    }

    const onErrorProject = (e) => {
        e.target.src = noProject;
        e.target.onerror = null;
    }
    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    if (loading) {
        return (
            <div className="text-center" style={{ position: 'relative', width: '100%', height: '100%', zIndex: '99', backgroundColor: 'rgba(255,255,255, 0.7)', justifyContent: 'center', alignItems: 'center' }}>
                <div
                    style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        background: "rgba(255,255,255, 0.5)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Spinner style={{ width: 48, height: 48 }} />
                </div>
            </div>
        )
    }

    if (dataUserListed.find(item => item.id === user.id)) {
        history.push("/project/" + matchRoute.params.code)
    }

    return (
        <div className="mx-auto solving-mobile-container">
            {!isSmallSize && <SolvingIcon message={true} team={false} finish={false} mb={5} />}
            <Card className="card-project-detail mx-auto">
                {!isSmallSize &&
                    <CardHeader className="bg-white pl-4 pr-2 pb-3">
                        <div className="d-flex mt-3">
                            <div className="text-left pr-md-0">
                                {data?.user?.photo ?
                                    <img src={data?.user?.photo} alt="profile" className="profile-photo-project rounded-circle" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                    :
                                    <DefaultProfile init={data?.user?.name} size="50px" />
                                }
                            </div>
                            <div className="text-left px-3 w-75">
                                <b>{data.user?.name}</b><br />
                                <div className="text-secondary">
                                    {data.locationName}
                                </div>
                            </div>
                        </div>
                    </CardHeader>
                }
                <CardBody style={{ position: 'relative' }} className="text-left px-0">
                    <div className="px-4">
                        {isSmallSize &&
                            <span style={{ position: 'absolute', top: '6px', right: '8px' }} className="text-secondary">
                                Langkah 1 dari 3
                            </span>
                        }
                        <div className="desc-card-project mt-1">
                            <h5><b>{data.title}</b></h5>
                            <ReactMarkdown source={data.description} />
                        </div>
                    </div>
                    <div>
                        <Carousel
                            activeIndex={activeIndex}
                            next={next}
                            previous={previous}
                            // ride={false}
                            interval={false}
                            className="carousel-post"
                        >
                            {data.media.map((item, idx) => (
                                <CarouselItem
                                    onExiting={() => setAnimating(true)}
                                    onExited={() => setAnimating(false)}
                                    key={idx}
                                >
                                    {item.mimeType.includes('video') ?
                                        <div className="position-relative w-100 h-100" onClick={() => videoPlayed.id !== item.id ? handlePlayVideo(item.id) : handlePauseVideo()} style={{ cursor: 'pointer' }}>
                                            {videoPlayed.id !== item.id &&
                                                <FontAwesomeIcon className="position-absolute" style={{ top: '46%', left: '48%' }} icon="play" size="4x" color="white" />
                                            }
                                            <ReactPlayer url={item.storage} playing={videoPlayed.id === item.id ? true : false} width="100%" height='100%' />
                                        </div>
                                        :
                                        <img src={item.storage} alt={'media ' + (idx + 1)} width="100%" height="100%" style={{ objectFit: 'contain' }} onError={(e) => onErrorProject(e)} />
                                    }
                                </CarouselItem>
                            ))}
                            <CarouselIndicators items={data.media} activeIndex={activeIndex} onClickHandler={goToIndex} />
                            {data.media.length > 0 &&
                                <>
                                    {activeIndex !== 0 && <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />}
                                    {activeIndex !== data.media.length - 1 && <CarouselControl direction="next" directionText="Next" onClickHandler={next} />}
                                </>
                            }
                        </Carousel>
                    </div>
                    <Row className="mt-3 px-4">
                        <Col xs="12">
                            <Input type="textarea" rows="5" placeholder="Tuliskan deskripsi idemu..." className="mb-3" onChange={(e) => handleChangeMessage(e)} />
                        </Col>
                        <Col xs="12" className="mb-5">
                            <Link to={`/project/${data.code}/solving/team`} onClick={nextStep}>
                                <Button color="netis-color" size="md" className={`float-right ${isSmallSize && `w-100`}`} disabled={message ? false : true}>Selesaikan Masalah</Button>
                            </Link>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    )
}

export default SolvingMessage