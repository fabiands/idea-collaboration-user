import React, { useState, useCallback, useEffect, useMemo } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom';
import Select from "react-select";
import { toast } from 'react-toastify';
import { Spinner, Row, Col, Card, CardHeader, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Button, Badge, CardTitle } from 'reactstrap'
import profilePhotoNotFound from '../../../assets/img/no-photo.png';
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';
import { useAuthUser } from '../../../store';
import request from '../../../utils/request';
import { Feather } from 'react-web-vector-icons';

function TeamDetail({ leadId, data, loading, status, teamId, mutate }) {
    const user = useAuthUser()
    const history = useHistory();

    const [modal, setModal] = useState(false)
    const [modalData, setModalData] = useState(null)
    const [modalInvite, setModalInvite] = useState({
        isOpen: false,
        position: 'bottom'
    })
    const toggle = (e) => {
        setModal(false)
    }

    const toggleInvite = () => {
        setModalInvite(old => ({ ...old, isOpen: false }))
    }

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    const selectStatus = [
        { value: 'pending', label: 'Pending' },
        { value: 'approved', label: 'Approved' }
    ]

    const changeStatus = useCallback((e) => {
        history.push('?status=' + e.value + '#myteam')
    }, [history])

    return (
        <Card className="design-sprint shadow-sm border-0">
            <CardHeader className="design-sprint-header">
                <CardTitle className="d-flex justify-content-between align-items-center mb-0">
                    <div className="font-lg font-weight-bold">Detail Team</div>
                    {leadId === user.id && <Button color="primary" size="sm" className="rounded d-none d-lg-block" onClick={() => setModalInvite({ isOpen: true, position: 'center' })}>Undang Anggota</Button>}
                </CardTitle>
            </CardHeader>
            <CardBody style={{ minHeight: '70vh' }}>
                {loading ?
                    <div
                        style={{
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                            background: "rgba(255,255,255, 0.5)",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            height: "75vh",
                        }}
                    >
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                    :
                    <Row>
                        <Col sm={{ size: '3', offset: 9 }}>
                            <Select
                                className="mb-3"
                                name="jobtype"
                                id="jobtype"
                                options={selectStatus}
                                onChange={changeStatus}
                                value={selectStatus.filter((s) => s.value === (status ?? 'approved'))}
                            />
                        </Col>
                        {data.filter((d) => d.status === status).map((member, idx) => (
                            <Col xs="12" md="6" lg="6" xl="4" key={idx}>
                                <Card className="border-0 card-member" onClick={() => {
                                    setModalData(member)
                                    setModal(true)
                                }}>
                                    <CardBody>
                                        <Row>
                                            <Col xs="3">
                                                <div className="rounded-circle lead-photo mb-3 d-flex justify-content-center align-items-center">
                                                    {member?.user?.photo ?
                                                        <img src={member?.user?.photo} alt="profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                                        :
                                                        <DefaultProfile init={member?.user?.fullName} size={100} />
                                                    }
                                                </div>
                                            </Col>
                                            <Col xs="9">
                                                <div className="py-1 px-5">
                                                    <b>{member.user.fullName}</b>
                                                    <p className="text-muted sprint-solving">{member.solving.message ?? 'Tidak ada ide'}</p>
                                                </div>
                                                {leadId === member.user.id &&
                                                    <div className="float-right">
                                                        <Badge size="sm" color="success">Leader</Badge>
                                                    </div>
                                                }
                                            </Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                }
                <ModalDetail data={modalData} isOpen={modal} toggle={(e) => toggle(e)} leadId={leadId} teamId={teamId} mutate={() => mutate()} />
                <ModalInvite isOpen={modalInvite.isOpen} toggle={(e) => toggleInvite(e)} teamId={teamId} mutate={() => mutate()} position={modalInvite.position} />
                <div style={{ zIndex: 999 }} className="d-block d-lg-none">
                    <div className="fab shadow" onClick={() => setModalInvite({ isOpen: true, position: 'bottom' })}>
                        <div className="fab-icon">
                            <Feather
                                name='plus'
                                color="#fff"
                                size={28}
                            />
                        </div>
                    </div>
                </div>
            </CardBody>
        </Card >
    )
}

const ModalDetail = ({ data, isOpen, toggle, teamId, mutate, leadId }) => {
    const user = useAuthUser()
    const [modalVerify, setModalVerify] = useState(false)
    const [modalVerifyData, setModalVerifyData] = useState(null)

    const toggleVerify = (e) => {
        setModalVerify(false)
        toggle(false)
    }

    const handleToggle = () => {
        toggle(false)
    }

    return (
        <>
            <Modal isOpen={isOpen} toggle={() => handleToggle()}>
                <ModalHeader toggle={() => handleToggle()}>Pemecahan masalah dari {data?.user?.fullName}</ModalHeader>
                <ModalBody>
                    <Row>
                        <Col xs="12">
                            <div>
                                {data?.solving?.message ?? 'Tidak ada ide'}
                            </div>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    {data?.status === 'pending' ?
                        <Row className="bd-highlight float-right" style={{ zIndex: 99 }}>
                            <Col xs="6" className="py-0 px-1 bd-highlight">
                                <Button
                                    color="netis-success"
                                    size="md"
                                    onClick={() => {
                                        setModalVerify(true);
                                        setModalVerifyData({ status: "approved", id: data.id });
                                    }}
                                >
                                    Terima
                                </Button>
                            </Col>
                            <Col xs="6" className="py-0 px-1 bd-highlight">
                                <Button
                                    color="netis-danger"
                                    size="md"
                                    onClick={() => {
                                        setModalVerify(true);
                                        setModalVerifyData({ status: "rejected", id: data.id });
                                    }}
                                >
                                    Tolak
                                </Button>
                            </Col>
                        </Row>
                        :
                        <>
                            {leadId !== data?.user?.id && leadId === user.id &&
                                <Button color="netis-danger" onClick={() => {
                                    setModalVerify(true);
                                    setModalVerifyData({ status: "approved", id: data.user.id, teamId: teamId });
                                }}>
                                    Keluarkan
                                </Button>
                            }
                            <Button className="mr-2" color="netis-secondary" onClick={() => handleToggle()}>
                                Tutup
                            </Button>
                        </>
                    }
                </ModalFooter>
            </Modal>
            <ModalVerify data={modalVerifyData} isOpen={modalVerify} toggle={(e) => toggleVerify(e)} type={data?.status === 'pending' ? 'verify' : 'kick'} mutate={() => mutate()} />
        </>
    )
}

const ModalVerify = ({ data, isOpen, toggle, type, mutate }) => {
    const handleToggle = () => {
        toggle(false)
    }

    const updateStatus = (status, id) => {
        request.put(`v1/teams/member/${id}`, { status })
            .then(() => {
                toast.success(`Berhasil ${status === 'approved' ? 'menerima' : 'menolak'} anggota`)
                toggle(false)
                mutate()
            })
            .catch(() => {
                toast.error('Verifikasi anggota gagal, Silahkan coba lagi')
                return;
            })
        // .finally(() => setSubmitting(false))
    }

    const kickMember = (status, id, teamId) => {
        request.delete(`v1/teams/${teamId}/member/${id}`, { status })
            .then(() => {
                toast.success('Berhasil mengeluarkan anggota dari tim')
                toggle(false)
                mutate()
            })
            .catch(() => {
                toast.error('Gagal mengeluarkan anggota dari tim, silahkan coba lagi')
                return;
            })
        // .finally(() => setSubmitting(false))
    }

    return (
        <Modal isOpen={isOpen} toggle={() => handleToggle()}>
            <ModalBody>
                <Row>
                    <Col xs="12">
                        {type === 'verify' ?
                            <p>Apa anda yakin akan {data?.status === 'approved' ? 'menerima' : 'menolak'} anggota ini ?</p>
                            :
                            <p>Apa anda yakin akan mengeluarkan anggota ini dari tim ?</p>
                        }
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button className="mr-2" color="netis-secondary" onClick={() => handleToggle()}>
                    Batal
                </Button>
                <Button color="netis-primary" onClick={() => type === 'verify' ? updateStatus(data.status, data.id) : kickMember(data.status, data.id, data.teamId)}>
                    Ya
                </Button>
            </ModalFooter>
        </Modal>
    )
}

const ModalInvite = ({ isOpen, toggle, teamId, mutate, position }) => {
    const [member, setMember] = useState([]);
    const [loading, setLoading] = useState(true);
    const [dataUser, setDataUser] = useState([]);
    const [dataUserListed, setDataUserListed] = useState([]);
    const matchRoute = useRouteMatch();
    const authUser = useAuthUser();

    useEffect(() => {
        request.get('v1/projects/' + matchRoute.params.code + '/users').then(res => {
            const user = res.data.data.filter((u) => u.id !== authUser.id)
            setDataUserListed(user);
        }).finally(() => setLoading(false))

        request.get('v1/users?emailMasking=true').then(res => {
            const user = res.data.data.filter((u) => u.id !== authUser.id)
            setDataUser(user);
        }).finally(() => setLoading(false))
    }, [authUser, matchRoute]);

    const postInvite = () => {
        request.post(`v1/teams/${teamId}/member`, { userId: member })
            .then(() => {
                toast.success(`Berhasil mengundang anggota ke dalam tim`)
                toggle(false)
                mutate()
            })
            .catch(() => {
                toast.error('Gagal mengundang anggota ke dalam tim')
                return;
            })
        // .finally(() => setSubmitting(false))
    }

    const handleToggle = () => {
        toggle(false)
    }

    const options = useMemo(() => {
        const opt = []
        // eslint-disable-next-line
        dataUser.map((v) => {
            const dataOptions = dataUserListed.find(u => u.id === v.id);
            if (!dataOptions)
                opt.push({ value: v.id, label: v.detail.fullName })
        })

        return opt
    }, [dataUser, dataUserListed])

    const handleChangeMember = (e) => {
        const opt = []
        if (e) {
            // eslint-disable-next-line
            e.map(v => {
                opt.push(v.value)
            })
        }
        setMember(opt)
    }

    return (
        <>
            <Modal className={`${position === 'bottom' ? 'bottom-small' : ''}`} isOpen={isOpen} toggle={() => handleToggle()}>
                <ModalHeader toggle={() => handleToggle()}>
                    <div className="font-lg font-weight-bold">Undang Anggota</div>
                </ModalHeader>
                <ModalBody className="py-5 px-4">
                    <Row>
                        <Col xs="12">
                            <p>Pilih anggota yang ingin anda undang ke dalam tim.</p>
                            <Select
                                closeMenuOnSelect={false}
                                options={options}
                                isClearable
                                isMulti
                                isLoading={loading}
                                placeholder="Pilih anggota..."
                                onChange={(e) => handleChangeMember(e)}
                                components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }} />
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="netis-primary" onClick={() => postInvite()}>
                        Undang
                    </Button>
                    <Button className="mr-2" color="netis-secondary" onClick={() => handleToggle()}>
                        Tutup
                    </Button>
                </ModalFooter>
            </Modal>
        </>
    )
}
export default TeamDetail;