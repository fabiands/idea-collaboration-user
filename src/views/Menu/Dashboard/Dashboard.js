import React, { useState, useEffect } from 'react'
import { Col, Row, Card, CardBody, CardHeader, CardFooter, Spinner } from 'reactstrap'
import LoadingAnimation from '../../../components/LoadingAnimation';
import { useAuthUser } from '../../../store';
import request from '../../../utils/request';
import ProjectCard, { badgeStatus } from '../Project/ProjectCard';
import noProject from '../../../assets/img/no-project.png';
import { Link } from 'react-router-dom';
import { MemberItem } from '../Project/ProjectDetail';
import SearchComponent from "../../../views/Menu/Project/Search/SearchComponent";
import { DefaultProfile } from '../../../components/Initial/DefaultProfile';
import InfiniteScroll from 'react-infinite-scroll-component';
import ModalCopyLink from '../../../components/ModalCopyLink';

function Dashboard() {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true);
  const [loadingReview, setLoadingReview] = useState(true);
  const [result, setResult] = useState([]);
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);
  const [modalCopyLink, setModalCopyLink] = useState({
    link: '',
    isOpen: false,
  })
  const user = useAuthUser();

  const toggleModalCopyLink = (link) => {
    setModalCopyLink({ link: link, isOpen: !modalCopyLink.isOpen });
  }

  useEffect(() => {
    request.get('v1/teams/me?status=approved')
      .then((res) => {
        setData(res.data.data)
      })
      // .catch(() => setError(true))
      .finally(() => setLoadingReview(false))
  }, []);

  useEffect(() => {
    request.get('v1/projects?verified=verified&page=' + page ?? 0).then(res => {
      if (res.data.data.length === 0) setHasMore(false)
      setResult(r => r.concat(res.data.data));
    }).finally(() => setLoading(false))
  }, [page])

  const onErrorImage = (e) => {
    e.target.src = noProject;
    e.target.onerror = null;
  }

  const fetchMoreData = () => {
    setTimeout(() => {
      setPage(p => p + 1)
    }, 1500);
  };

  const refreshData = () => {
    setTimeout(() => {
      setResult([])
      setPage(0)
    }, 1500);
  };

  if (loading || loadingReview) {
    return <LoadingAnimation />
  }

  return (
    <div className="dashboard-page text-center container">
      <Row className="mt-md-3 mt-lg-n2">
        <Col xs="12" md="8" xl="7" className="p-0 px-md-3">
          <InfiniteScroll
            dataLength={result.length}
            next={fetchMoreData}
            hasMore={hasMore}
            loader={<Spinner style={{ width: 35, height: 35 }} className="my-1" />}
            refreshFunction={refreshData}
            style={{ overflow: 'unset' }}
          // pullDownToRefresh
          // pullDownToRefreshThreshold={50}
          // pullDownToRefreshContent={
          //   <h3 style={{ textAlign: 'center' }}>&#8595; Pull down to refresh</h3>
          // }
          // releaseToRefreshContent={
          //   <h3 style={{ textAlign: 'center' }}>&#8593; Release to refresh</h3>
          // }
          >
            {result.map((item, idx) => (
              <div key={idx} className="my-2">
                <ProjectCard data={item} onClickCopyLink={(link) => toggleModalCopyLink(link)} />
              </div>
            ))}
          </InfiniteScroll>
        </Col>
        <Col xs="4" className="d-none d-md-block text-left profile-review">
          <SearchComponent data={result} />
          <Card className="shadow-sm mt-3" style={{ borderRadius: '5px' }}>
            <CardHeader className="px-3 bg-white border-bottom-0">
              <div className="my-2 d-flex align-items-center">
                {user?.detail.photo ?
                  <img src={user?.detail.photo} alt="profile" className="profile-photo-review rounded-circle" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                  :
                  <DefaultProfile init={user.detail.fullName} size="60px" />
                }
                <div>
                  <h6 className="font-weight-bold ml-3">{user.detail.fullName}</h6>
                  <h6 className="text-muted ml-3 mb-0">{user.email}</h6>
                </div>
              </div>
              <div className="d-flex justify-content-between mb-2">
                <h6 className="font-weight-bold text-muted">Project yang anda ikuti</h6>
                <h6 className="font-weight-bold">
                  <Link
                    to={`/profile?show=myteams`}
                    style={{ color: 'black' }}
                  >Lihat Semua
                  </Link>
                </h6>
              </div>
            </CardHeader>
            <CardBody className="py-1 px-3" style={{ overflowY: 'scroll', maxHeight: '50vh' }}>
              <div>
                {data.filter(item => item.status === 'approved').map((item, idx) => (
                  <Card className="card-team-review mb-1 border-0" style={{ borderRadius: '5px' }} key={idx}>
                    <CardBody className="py-2 px-0">
                      <Row className="card-team-info">
                        <Col xs="12" className="d-flex mb-2">
                          <img src={item?.project.imagePreview.url} alt="profile" className="project-photo-review rounded" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                          <div>
                            <Link
                              to={{
                                pathname: `/project/${item.project.code}`,
                                // search: `?team=${item.lead.leadId}`,
                                state: { team: item.lead.leadName }
                              }}
                              style={{ color: 'black' }}
                            >
                              <h6 className="font-weight-bold ml-3">{item.project.title}</h6>
                            </Link>
                            <Link
                              to={{
                                pathname: `/project/${item.project.code}/team/${item.id}`,
                                // search: `?team=${item.lead.leadId}`,
                                state: { team: item.lead.leadName }
                              }}
                              style={{ color: 'black' }}
                            >
                              <h6 className="text-muted ml-3 mb-0">Tim {item.lead.leadName}</h6>
                            </Link>
                          </div>
                        </Col>
                        <Col xs="6">
                          <div className="d-flex flex-column flex-lg-fill float-left">
                            <span className="text-muted text-left">Status Proyek</span>
                            <div className="d-flex align-items-center mt-2">
                              {badgeStatus(item.project.status)}
                            </div>
                          </div>
                        </Col>
                        <Col xs="6">
                          <div className="d-flex flex-column flex-lg-fill float-right">
                            <span className="text-muted text-right">Members</span>
                            <div className="symbol-group symbol-hover">
                              {item.members.filter(m => m.status === 'approved').map((member, k) => (
                                k >= 3 ? null :
                                  <MemberItem member={member} project={item.project} key={k} />
                              ))}
                              {item.members.length > 3 &&
                                <div className="symbol symbol-30 symbol-circle symbol-light">
                                  <span className="symbol-label font-weight-bold">{item.members.length - 3}+</span>
                                </div>
                              }
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                ))}
              </div>
            </CardBody>
            <CardFooter className="py-4 bg-white border-top-0">
            </CardFooter>
          </Card>
        </Col>
      </Row>
      <ModalCopyLink data={modalCopyLink} toggle={() => toggleModalCopyLink('')} />
    </div>
  )
}

export default Dashboard