import React from 'react';
import Illustration from '../assets/assets_ari/notfound.png';

const DataNotFound = () => {
    return (
        <div className="text-center my-5">
            <img src={Illustration} alt="not found" />
        </div>
    );
}

export default DataNotFound;
