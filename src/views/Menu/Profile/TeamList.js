import React, { useMemo, useState } from 'react'
import { Button, Card, CardBody, Col, Row } from 'reactstrap'
import { Link } from 'react-router-dom';
import projectNoImage from '../../../assets/img/no-project.png';
import useSWR from 'swr';
import { badgeStatus } from '../Project/ProjectCard';
import { MemberItem } from '../Project/ProjectDetail';

const noDataDesc = {
    pending: 'belum terverifikasi',
    approved: 'disetujui',
    rejected: 'ditolak'
}

function Profile() {
    const [filter, setFilter] = useState('rejected')
    const { data: getMyTeam, error: getMyTeamError } = useSWR('v1/teams/me', { refreshInterval: 15000 });
    const dataMyTeam = useMemo(() => getMyTeam?.data?.data ?? [], [getMyTeam]);

    const onErrorImage = (e) => {
        e.target.src = projectNoImage;
        e.target.onerror = null;
    }

    return (
        <Row>
            <Col xs="12" className="d-flex justify-content-center mb-3">
                <Button color={filter === 'pending' ? 'primary' : 'secondary'} className="mx-1">
                    Belum Verifikasi
                </Button>
                <Button color={filter === 'approved' ? 'primary' : 'secondary'} className="mx-1">
                    Disetujui
                </Button>
                <Button color={filter === 'rejected' ? 'primary' : 'secondary'} className="mx-1">
                    Ditolak
                </Button>
            </Col>
            {!getMyTeamError ?
                dataMyTeam?.length < 1 ?
                    <Col xs="12" className="pt-4 mx-auto text-center">
                        Belum ada Tim yang {noDataDesc[filter]}
                    </Col>
                :
                dataMyTeam.map((item, i) => (
                    <Col xs="12" md="6" key={i} className={`p-2`}>
                        <Link
                            to={{
                                pathname: `/project/${item.project.code}/team/${item.id}`,
                                // search: `?team=${item.lead.leadId}`,
                                state: { team: item.lead.leadName }
                            }}
                            style={{ color: 'black' }}
                        >
                            <Card className="card-team-review mb-1" style={{ borderRadius: '5px' }}>
                                <CardBody>
                                    <Row className="card-team-info">
                                        <Col xs="12" className="d-flex mb-2">
                                            <img src={item?.project.imagePreview.url} alt="profile" className="project-photo-review rounded" onError={(e) => onErrorImage(e)} style={{ objectFit: 'cover' }} />
                                            <div>
                                                <h6 className="font-weight-bold ml-3">{item.project.title}</h6>
                                                <h6 className="text-muted ml-3 mb-0">Tim {item.lead.leadName}</h6>
                                            </div>
                                        </Col>
                                        <Col xs="6">
                                            <div className="d-flex flex-column flex-lg-fill float-left">
                                                <span className="text-muted text-left">Status Proyek</span>
                                                <div className="d-flex align-items-center mt-2">
                                                    {badgeStatus(item.project.status)}
                                                </div>
                                            </div>
                                        </Col>
                                        <Col xs="6">
                                            <div className="d-flex flex-column flex-lg-fill float-right">
                                                <span className="text-muted text-right">Members</span>
                                                <div className="symbol-group symbol-hover">
                                                    {item.members.filter(m => m.status === 'approved').map((member, k) => (
                                                        k >= 3 ? null :
                                                            <MemberItem member={member} project={item.project} key={k} />
                                                    ))}
                                                    {item.members.length > 3 &&
                                                        <div className="symbol symbol-30 symbol-circle symbol-light">
                                                            <span className="symbol-label font-weight-bold">{item.members.length - 3}+</span>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Link>
                    </Col>
            )): null}
        </Row>
    )
}

export default Profile